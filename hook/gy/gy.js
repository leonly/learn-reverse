/*
 * @Descripttion: 
 * @version: 
 * @Author: Leo
 * @Date: 2021-01-26 17:57:42
 * @LastEditor: Leo
 * @LastEditTime: 2021-01-26 22:34:15
 */

function hook_sig(deviceKey) {
    var result;
    Java.perform(function () {
        var class_old = Java.use("com.tgc.sky.SystemIO_android");
        result = class_old.getInstance().SignWithDeviceKey(deviceKey);
    });
    return result;
}

rpc.exports = {
    hookSig: hook_sig,
};