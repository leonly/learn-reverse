/*
 * @Author: Leo
 * @Date: 2021-08-23 19:29:19
 * @LastEditors: Leo
 * @LastEditTime: 2021-08-23 20:12:30
 * @Description: 
 */

console.log("--------------------------start---------------------------");
// var a = Process.enumerateModulesSync();
var baseaddr = Process.findModuleByName('libgame.so').base;
console.log("soBaseAddr:" + baseaddr);

// var getChecksum = ptr(parseInt(baseaddr) + 0x39DD70);
// Interceptor.attach(getChecksum, {
//     onEnter: function (args) {
//         console.log("enter getChecksum fun..........")
//         console.log("x0: " + hexdump(this.context.x0));
//         console.log("x1: " + hexdump(this.context.x1));
//     },
//     onLeave: function (retVal) {
//         console.log("retVal: " + hexdump(retVal));
//     }
// });

var getMd5Hash = ptr(parseInt(baseaddr) + 0xBA90D0);
Interceptor.attach(getMd5Hash, {
    onEnter: function (args) {
        console.log("enter getMd5Hash fun..........")
        console.log("x0: " + hexdump(this.context.x0));
        console.log("x1: " + hexdump(this.context.x1));
    },
    onLeave: function (retVal) {
        console.log("retVal: " + hexdump(retVal));
    }
});