console.log("--------------------------start---------------------------");
var a = Process.enumerateModulesSync();
var baseaddr = Process.findModuleByName('libil2cpp.so').base;
console.log("soBaseAddr:" + baseaddr);


//获取EcptcheckInt补天石get，set函数
var get_Value = parseInt(baseaddr) + 0x1b355e0;
var get_Valuenp = new NativePointer(get_Value);
console.log(Memory.readByteArray(get_Valuenp, 0x20));
var get_Valuenp_func = new NativeFunction(get_Valuenp, 'pointer', ['pointer']);


var set_Value = parseInt(baseaddr) + 0x3a08c98;
var set_Valuenp = new NativePointer(set_Value);
console.log(Memory.readByteArray(set_Valuenp, 0x20));
var set_Valuenp_func = new NativeFunction(set_Valuenp, 'pointer', ['pointer', 'int']);


Interceptor.attach(get_Valuenp_func, {
	onEnter: function (args) {
		console.log("-----enter get value page:");
	},
	onLeave: function (retVal) {
		var r = parseInt(this.context.x0);
		console.log("-----leave get value*********** page:");
		console.log("x0: " + parseInt(this.context.x0));
	}
})

//获取EcptInt补天石get，set函数
// var get_Value2 = parseInt(baseaddr) + 0x17d46c0;
// var get_Valuenp2 = new NativePointer(get_Value2);
// console.log(Memory.readByteArray(get_Valuenp2, 0x20));
// var get_Valuenp_func2 = new NativeFunction(get_Valuenp2, 'pointer', ['pointer']);


// var set_Value2 = parseInt(baseaddr) + 0x17d30d0;
// var set_Valuenp2 = new NativePointer(set_Value2);
// console.log(Memory.readByteArray(set_Valuenp2, 0x20));
// var set_Valuenp_func2 = new NativeFunction(set_Valuenp2, 'pointer', ['pointer', 'int']);




//0x15212D0这个点会对savedata做序列化，这儿hook目的是为了获取savedata

// var Func_address3 = parseInt(baseaddr) + 0x15212D0;
// var Func_np3 = new NativePointer(Func_address3);
// console.log(Memory.readByteArray(Func_np3, 0x20));
// var friendlyFunctionName3 = new NativeFunction(Func_np3, 'pointer', ['pointer']);
// Interceptor.attach(friendlyFunctionName3, {
// 	onEnter: function (args) {
// 		console.log(Memory.readByteArray(this.context.x0, 1000))
// 		//x0是savedata，加0x28后是profile对象，profile对象再加0x30后是EcptCheckInt2对象，也就是和补天石相关的类，然后主动调用该EcptCheckInt2里的setvalue给补天石设置值
// 		var m_profile = ptr(ptr(parseInt(this.context.x0) + 0x28).readU64());
// 		var m_gem = ptr(ptr(parseInt(m_profile) + 0x30).readU64());
// 		var m_checkgem = ptr(ptr(parseInt(m_profile) + 0xf8).readU64());
// 		var aaa = get_Valuenp_func(m_gem);
// 		console.log("-------checkgem--------")
// 		console.log(aaa)
// 		//set_Valuenp_func(m_gem,30000);
// 		var bbb = get_Valuenp_func2(m_checkgem);
// 		console.log("-------gem--------")
// 		console.log(bbb)
// 		set_Valuenp_func2(m_checkgem, 30000 * 9);
// 		set_Valuenp_func(m_gem, 30000);
// 	},
// 	onLeave: function (retval) {

// 	}
// });


//gem和checkgem对比是否相等的点
// var Func_address33 = parseInt(baseaddr) + 0x18DBEA0;
// var Func_np33 = new NativePointer(Func_address33);
// console.log(Memory.readByteArray(Func_np33, 0x20));
// var friendlyFunctionName33 = new NativeFunction(Func_np33, 'pointer', ['pointer', 'pointer', 'pointer']);
// Interceptor.attach(friendlyFunctionName33, {
// 	onEnter: function (args) {
// 		console.log("---------equale---------");
// 		console.log(args[0]);
// 		console.log(args[1]);
// 		console.log(args[2]);

// 	},
// 	onLeave: function (retval) {

// 	}
// });