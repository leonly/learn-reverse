/*
 * @Author: Leo
 * @Date: 2021-04-12 22:34:46
 * @LastEditors: Leo
 * @LastEditTime: 2021-04-14 23:23:14
 * @Description: 
 */

console.log("--------------------------start---------------------------")
var a = Process.enumerateModulesSync()
var baseaddr = Process.findModuleByName('libil2cpp.so').base
console.log("soBaseAddr:" + baseaddr)

//补天石
var v1 = 90000
var v1Num = 109


var check_int_address2 = parseInt(baseaddr) + 0x17d3974;
var check_int2 = new NativePointer(check_int_address2);

var set2_addr = parseInt(baseaddr) + 0x17d30d0;
var set2 = new NativePointer(set2_addr);

var safeuploadsaver = parseInt(baseaddr) + 0x28193E8;
var saver = new NativePointer(safeuploadsaver);

var check_int_store = parseInt(baseaddr) + 0x17d7878;
var store = new NativePointer(check_int_store);

var check_int_implicit = parseInt(baseaddr) + 0x17d47d8;
var implicit = new NativePointer(check_int_implicit);

var check_int_implicit2 = parseInt(baseaddr) + 0x17d48a0;
var implicit2 = new NativePointer(check_int_implicit2);


var ctor = parseInt(baseaddr) + 0x17d31f4;
var cctor = new NativePointer(ctor);

var modifypageend = parseInt(baseaddr) + 0x17D4570; //0x17D3274;
var modifyend = new NativePointer(modifypageend);

var getInitp = parseInt(baseaddr) + 0x13DDA2C;
var getinit = new NativePointer(getInitp);

var mathmini = parseInt(baseaddr) + 0x17C4DE0; //0x18DC058;
// var mathmini = parseInt(baseaddr) + 0x18DBFD4;
var mathm = new NativePointer(mathmini);

Interceptor.attach(mathm, {
    onEnter: function (args) {
        console.log("-----___________________________enter math *********** page:");
        var p = parseInt(this.context.x0);
        console.log("x0 : " + this.context.x0);
        console.log("x0 int: " + p);
        console.log("x1 : " + this.context.x1);
        console.log("x1 int: " + Memory.readU32(this.context.x1));
        console.log("x2 : " + this.context.x2);
        console.log("x2 int: " + Memory.readCString(this.context.x2, 8));
        console.log("x3 : " + this.context.x3);
        console.log("x3 int: " + Memory.readCString(this.context.x3, 8));
        console.log("x4 : " + this.context.x4);
        console.log("x4 int: " + Memory.readCString(this.context.x4, 8));
        console.log("x5 : " + this.context.x5);
        console.log("x5 int: " + Memory.readCString(this.context.x5, 8));
        // console.log("x1 int 2: " + Memory.readU32(this.context.x1));
        if (p > 4294940646) {
            // this.context.x0 = v1;
        }
        // this.context.x0 = v1;
        // console.log("x0 int: " + parseInt(this.context.x0));
        //x0是savedata，加0x28后是profile对象，profile对象再加0x30后是EcptCheckInt2对象，也就是和补天石相关的类，然后主动调用该EcptCheckInt2里的setvalue给补天石设置值
        // var m_profile=ptr(ptr(parseInt(this.context.x1)+0x28).readU64());
        // var m_gem=ptr(ptr(parseInt(m_profile)+0x30).readU64());
        // var m_checkgem=ptr(ptr(parseInt(m_profile)+0xf8).readU64());
        // console.log("m-profile: " + m_profile);
        // console.log("m-gem: " + m_gem);
        // console.log("m-checkgem: " + m_checkgem);
        // this.context.x0 = v1

        // console.log(Memory.readByteArray(this.context.x0, 1000))
        // console.log("x1 int: " + parseInt(this.context.x1));
        // console.log("x1 int: " + parseInt(this.context.x1));
        // console.log("x1 int: " + Memory.readU32(this.context.x21));
        // if (parseInt(this.context.x1) > 4000000000) {
        //     console.log("change valu in ctor");
        //     this.context.x1 = parseInt(this.context.x1) + v1;
        // }
    },
    onLeave: function (retVal) {
        // if (parseInt(retVal) > 4294856866 && parseInt(retVal) < 4294956866) {
        var r = parseInt(this.context.x0);
        if (r > 4293940426 && r < 4295953427) {
            console.log("-----___________________________leave math *********** page:");
            console.log("x0: " + parseInt(this.context.x0));
            // this.context.x0 = v1;
            console.log("x0: " + parseInt(this.context.x0));
        }
    }

})


Interceptor.attach(modifyend, {
    onEnter: function (args) {
        console.log("-----enter modify end page:");
        var d = parseInt(this.context.x0);
        console.log("x0 ******************************** end int: " + d);
        if (d < 4295977277 && d > 4293940386) {
            this.context.x0 = v1;
            console.log("x0 ******************************** end int: " + parseInt(this.context.x0));
            console.log('func called from:\n' +
                Thread.backtrace(this.context, Backtracer.ACCURATE)
                .map(DebugSymbol.fromAddress).join('\n') + '\n');
        }
    }
})


Interceptor.attach(implicit, {
    onEnter: function (args) {
        console.log("-----enter to_int32:");
        console.log("arg0 pointer: " + this.context.x0);
    },
    onLeave: function (retVal) {
        console.log("-----leave to_int32:");
        console.log(parseInt(this.context.x0));
        var arg = parseInt(this.context.x0);
        if (arg < 10) {
            this.context.x0 = v1;
        }
        if (arg < 4295967277 && arg > 4293940646) {
            console.log("-----change to_int32:");
            // this.context.x0 = v1;
            console.log(parseInt(this.context.x0));
        }
        // if (parseInt(this.context.x0) == 4294967256) {
        //     this.context.x0 = parseInt(this.context.x0) + v1 - 20;
        //     console.log(parseInt(this.context.x0));
        // }
    }
})

Interceptor.attach(store, {
    onEnter: function (args) {
        // var p1 = Memory.readU32(this.context.x0);
        // if (p1 > 4000000000) {
        var cc = parseInt(this.context.x1);
        if (cc > 20000 && cc < 200000) {
            console.log("-----enter store:");
            console.log("arg0 int: " + parseInt(this.context.x0));
            console.log("arg1 int: " + parseInt(this.context.x1));
            // this.context.x1 = v1;
        }

        if (cc < 4295967126 && cc > 4293967146) {
            console.log("-----enter store change:");
            console.log("arg0 int: " + parseInt(this.context.x0));
            console.log("arg1 int: " + parseInt(this.context.x1));
            this.context.x1 = v1;
        }
    },
    onLeave: function (retVal) {
        // console.log("-----leave store");
        // console.log("retVal: " + parseInt(retVal))
    }
})

Interceptor.attach(saver, {
    onEnter: function (args) {
        var p1 = Memory.readU32(this.context.x0);
        if (p1 > 48000 && p1 < 200000) {
            console.log("-----enter saver:" + p1);
            console.log("arg0 int: " + p1);
        }
        if (p1 > 4293967256) {
            console.log("-----enter saver:" + p1);
            console.log("arg0 int: " + p1);
            console.log("change:changechangechangechangechangechangechange");
            // console.log('func called from:\n' +
            //         Thread.backtrace(this.context, Backtracer.ACCURATE)
            //         .map(DebugSymbol.fromAddress).join('\n') + '\n');
            var np = new NativePointer(this.context.x0);

            np.writeU32(p1 + v1);
            p1 = Memory.readU32(this.context.x0);
            console.log("modify arg0 int: " + p1);
        }

    },
    onLeave: function (retVal) {
        // if (parseInt(retVal) > 4000000000 && parseInt(retVal) < 5000000000) { //v1
        //     console.log("enter functer getintvalue:.........")
        //     // console.log('func called from:\n' +
        //     //     Thread.backtrace(this.context, Backtracer.ACCURATE)
        //     //     .map(DebugSymbol.fromAddress).join('\n') + '\n');
        // console.log("-----leave saver:");
        //     // console.log(this.context.lr);
        // console.log(parseInt(retVal));
        //     this.context.x0 = v1;
        // }
    }
})

Interceptor.attach(set2, {
    onEnter: function (args) {
        var arg = parseInt(this.context.x1);
        // console.log("set2 arg0: " + Memory.readCString(args[0].add(10), 8))
        console.log("set2 arg1: " + arg)
        if (arg < 20) { //v1
            console.log("enter functer set_value2:.........")
            // console.log('func called from:\n' +
            //     Thread.backtrace(this.context, Backtracer.ACCURATE)
            //     .map(DebugSymbol.fromAddress).join('\n') + '\n');
            console.log("-----vset2: " + parseInt(this.context.x1));
            this.context.x1 = v1;
            // this.context.x1 = parseInt(this.context.x1) + v1;
        }
        if (arg < 4295967277 && arg > 4293940646) {
            console.log("-----vset2:");
            console.log('func called from:\n' +
                Thread.backtrace(this.context, Backtracer.ACCURATE)
                .map(DebugSymbol.fromAddress).join('\n') + '\n');
            this.context.x1 = v1;
            console.log("change:changechangechangechangechangechangechange: " + parseInt(this.context.x1));
        }
    },
    // onLeave: function (retVal) {
    //     console.log("leave set2" + parseInt(retVal));

    // }
})

Interceptor.attach(check_int2, {
    onEnter: function (args) {
        console.log("enter functer 22222222222222222:.........")
        console.log(parseInt(this.context.x0))
    },
    onLeave: function (retVal) {
        if (parseInt(retVal) < 4294956846 && parseInt(retVal) > 4294930746) { //v1
            console.log("leave functer 22222222222222222:.........")
            console.log('func called from:\n' +
                Thread.backtrace(this.context, Backtracer.ACCURATE)
                .map(DebugSymbol.fromAddress).join('\n') + '\n');
            console.log("-----check_int2:");
            // console.log(this.context.lr);
            console.log(parseInt(retVal));
            // this.context.x0 = parseInt(this.context.x0) + v1;
            this.context.x0 = v1;
        }

        if (parseInt(retVal) > 40000 && parseInt(retVal) < 400000) { //v1
            console.log("leave functer no change 22222222222222222:.........")
            console.log(parseInt(retVal));
        }
    }
})