console.log("--------------------------start---------------------------")
var a = Process.enumerateModulesSync()
var baseaddr = Process.findModuleByName('libil2cpp.so').base
console.log("soBaseAddr:" + baseaddr)

//补天石
var v1 = 80000
var v1Num = 1096
var getvalue = parseInt(baseaddr) + 0x17d46c0;
var setvalue = parseInt(baseaddr) + 0x17d798c;
var getintvalue = new NativePointer(getvalue);
var setintvalue = new NativePointer(setvalue);

var check_int_address1 = parseInt(baseaddr) + 0x18f6d60;
var check_int1 = new NativePointer(check_int_address1);

var set1_addr = parseInt(baseaddr) + 0x18f64bc;
var set1 = new NativePointer(set1_addr);

var check_int_address2 = parseInt(baseaddr) + 0x17d3974;
var check_int2 = new NativePointer(check_int_address2);

var set2_addr = parseInt(baseaddr) + 0x17d30d0;
var set2 = new NativePointer(set2_addr);

var safeuploadsaver = parseInt(baseaddr) + 0x28193E8;
var saver = new NativePointer(safeuploadsaver);
var deencry = parseInt(baseaddr) + 0x37CF1AC;
var dencry = new NativePointer(deencry);

var check_int_store = parseInt(baseaddr) + 0x17d7878;
var store = new NativePointer(check_int_store);

var check_int_implicit = parseInt(baseaddr) + 0x17d47d8;
var implicit = new NativePointer(check_int_implicit);

var check_int_implicit2 = parseInt(baseaddr) + 0x17d48a0;
var implicit2 = new NativePointer(check_int_implicit2);

var get_KEY = parseInt(baseaddr) + 0x17d32b0;
var getkey = new NativePointer(get_KEY);

var get_IV = parseInt(baseaddr) + 0x17d3610;
var getiv = new NativePointer(get_IV);

var ctor = parseInt(baseaddr) + 0x17d31f4;
var cctor = new NativePointer(ctor);

var modifypage = parseInt(baseaddr) + 0x17D325C;
var modifyp = new NativePointer(modifypage);

var modifypageend = parseInt(baseaddr) + 0x17D4570; //0x17D3274;
var modifyend = new NativePointer(modifypageend);

// var mathmini = parseInt(baseaddr) + 0x18DC058;
var mathmini = parseInt(baseaddr) + 0x18DBFD4;
var mathm = new NativePointer(mathmini);



Interceptor.attach(modifyend, {
    onEnter: function (args) {
        console.log("-----enter modify end page:");
        var d = parseInt(this.context.x0);
        console.log("x0 ******************************** end int: " + d);
        if (d < 4294953386 && d > 4294940386) {
            this.context.x0 = v1;
            console.log("x0 ******************************** end int: " + parseInt(this.context.x0));
            console.log('func called from:\n' +
                Thread.backtrace(this.context, Backtracer.ACCURATE)
                .map(DebugSymbol.fromAddress).join('\n') + '\n');
        }
    }
})

Interceptor.attach(mathm, {
    onEnter: function (args) {
        console.log("-----___________________________enter math *********** page:");
        var p = parseInt(this.context.x0);
        console.log("x0 : " + this.context.x0);
        console.log("x0 int: " + p);
        if (p > 4294940646) {
            // this.context.x0 = v1;
        }
        // console.log("x0 int: " + Memory.readU32(this.context.x0));
        // this.context.x0 = v1;
        // console.log("x0 int: " + parseInt(this.context.x0));
        //x0是savedata，加0x28后是profile对象，profile对象再加0x30后是EcptCheckInt2对象，也就是和补天石相关的类，然后主动调用该EcptCheckInt2里的setvalue给补天石设置值
        // var m_profile=ptr(ptr(parseInt(this.context.x1)+0x28).readU64());
        // var m_gem=ptr(ptr(parseInt(m_profile)+0x30).readU64());
        // var m_checkgem=ptr(ptr(parseInt(m_profile)+0xf8).readU64());
        // console.log("m-profile: " + m_profile);
        // console.log("m-gem: " + m_gem);
        // console.log("m-checkgem: " + m_checkgem);
        // this.context.x0 = v1

        // console.log(Memory.readByteArray(this.context.x0, 1000))
        // console.log("x1 int: " + parseInt(this.context.x1));
        // console.log("x1 int: " + parseInt(this.context.x1));
        // console.log("x1 int: " + Memory.readU32(this.context.x21));
        // if (parseInt(this.context.x1) > 4000000000) {
        //     console.log("change valu in ctor");
        //     this.context.x1 = parseInt(this.context.x1) + v1;
        // }
    },
    onLeave: function (retVal) {
        // if (parseInt(retVal) > 4294856866 && parseInt(retVal) < 4294956866) {
        // var r = parseInt(this.context.x0);
        // if (r > 4294950426 && r < 4294953427) {
        //     console.log("-----___________________________leave math *********** page:");
        //     console.log("x0: " + parseInt(this.context.x0));
        //     this.context.x0 = v1;
        //     console.log("x0: " + parseInt(this.context.x0));
        // }
        // }
    }
})

Interceptor.attach(cctor, {
    onEnter: function (args) {
        console.log("-----enter ctor :");
        console.log("arg0: " + parseInt(args[0]));
        // if (parseInt(this.context.x1) > 4000000000) {
        //     console.log("change valu in ctor");
        //     this.context.x1 = parseInt(this.context.x1) + v1;
        // }
    },
    onLeave: function (retVal) {
        console.log("-----leave ctor:");
    }
})

Interceptor.attach(getkey, {
    onEnter: function (args) {

        console.log("-----enter get_key:");
        // this.context.x1 = v1;
    },
    onLeave: function (retVal) {
        console.log("-----leave get_key:");
        console.log(parseInt(retVal));

    }
})

Interceptor.attach(getiv, {
    onEnter: function (args) {
        console.log("-----enter get_iv:");
        // this.context.x1 = v1;

    },
    onLeave: function (retVal) {
        console.log("-----leave get_iv:");
        console.log(parseInt(retVal));

    }
})

Interceptor.attach(implicit2, {
    onEnter: function (args) {
        // console.log("-----***********************************enter implicit_op_equality:");
        // console.log(Memory.readByteArray(this.context.x0, 100))
        // console.log("--------------------------------------------------------------------");
        // console.log(Memory.readByteArray(this.context.x1, 100))
        // console.log("arg0 int: " + Memory.readCString(args[0].add(10), 16));
        // console.log("arg1 int: " + Memory.readCString(this.add(10), 8));
        // this.context.x1 = v1;
    },
    onLeave: function (retVal) {
        console.log("-----leave implicit_op_equality:");
        console.log(parseInt(retVal));
    }
})

Interceptor.attach(implicit, {
    onEnter: function (args) {
        console.log("-----enter to_int32:");
        console.log("arg0 pointer: " + this.context.x0);
    },
    onLeave: function (retVal) {
        console.log("-----leave to_int32:");
        console.log(parseInt(this.context.x0));
        var arg = parseInt(this.context.x0);
        if (arg < 4295946847 && arg > 4293940646) {
            this.context.x0 = v1;
            console.log(parseInt(this.context.x0));
        }
        // if (parseInt(this.context.x0) == 4294967256) {
        //     this.context.x0 = parseInt(this.context.x0) + v1 - 20;
        //     console.log(parseInt(this.context.x0));
        // }
    }
})

Interceptor.attach(store, {
    onEnter: function (args) {
        // var p1 = Memory.readU32(this.context.x0);
        // if (p1 > 4000000000) {
        var cc = parseInt(this.context.x1);
        if (cc > 20000 && cc < 200000) {
            console.log("-----enter store:");
            console.log("arg0 int: " + parseInt(this.context.x0));
            console.log("arg1 int: " + parseInt(this.context.x1));
            // this.context.x1 = v1;
        }

        if (cc > 4000000000) {
            console.log("-----enter store change:");
            console.log("arg0 int: " + parseInt(this.context.x0));
            console.log("arg1 int: " + parseInt(this.context.x1));
            // this.context.x1 = v1;
        }
    },
    onLeave: function (retVal) {
        // console.log("-----leave store");
        // console.log("retVal: " + parseInt(retVal))
    }
})

Interceptor.attach(saver, {
    onEnter: function (args) {
        var p1 = Memory.readU32(this.context.x0);
        if (p1 > 50000 && p1 < 200000) {
            console.log("-----enter saver:" + p1);
            console.log("arg0 int: " + p1);
        }
        if (p1 > 4293967256) {
            console.log("-----enter saver:" + p1);
            console.log("arg0 int: " + p1);
            console.log("change:changechangechangechangechangechangechange");
            // console.log('func called from:\n' +
            //         Thread.backtrace(this.context, Backtracer.ACCURATE)
            //         .map(DebugSymbol.fromAddress).join('\n') + '\n');
            var np = new NativePointer(this.context.x0);

            np.writeU32(p1 + v1);
            p1 = Memory.readU32(this.context.x0);
            console.log("modify arg0 int: " + p1);
        }

    },
    onLeave: function (retVal) {
        // if (parseInt(retVal) > 4000000000 && parseInt(retVal) < 5000000000) { //v1
        //     console.log("enter functer getintvalue:.........")
        //     // console.log('func called from:\n' +
        //     //     Thread.backtrace(this.context, Backtracer.ACCURATE)
        //     //     .map(DebugSymbol.fromAddress).join('\n') + '\n');
        // console.log("-----leave saver:");
        //     // console.log(this.context.lr);
        // console.log(parseInt(retVal));
        //     this.context.x0 = v1;
        // }
    }
})

Interceptor.attach(getintvalue, {
    onEnter: function (args) {
        // console.log("-----enter getintvalue:");
        // console.log("arg0: " + this.context.x0)
        // console.log("arg1: " + parseInt(this.context.x1))
    },
    onLeave: function (retVal) {
        if (parseInt(retVal) > 4293950646) { //v1
            console.log("leave functer getintvalue:.........")
            // console.log('func called from:\n' +
            //     Thread.backtrace(this.context, Backtracer.ACCURATE)
            //     .map(DebugSymbol.fromAddress).join('\n') + '\n');
            // console.log(this.context.lr);
            console.log(parseInt(retVal));
            // this.context.x0 = v1;
        }
    }
})
Interceptor.attach(setintvalue, {
    onEnter: function (args) {
        console.log("-----enter setintvalue:");
        console.log("arg0: " + this.context.x0)
        console.log("arg1: " + parseInt(this.context.x1))
        // this.context.x1 = v1;
    },
    onLeave: function (retVal) {
        if (parseInt(retVal) > 400000) { //v1
            console.log("enter functer setintvalue:.........")
            // console.log('func called from:\n' +
            //     Thread.backtrace(this.context, Backtracer.ACCURATE)
            //     .map(DebugSymbol.fromAddress).join('\n') + '\n');
            console.log("-----leave setintvalue:");
            // console.log(this.context.lr);
            console.log(parseInt(retVal));
            // this.context.x0 = v1;
        }
    }
})

// var friendlyFunctionName3 = new NativeFunction(Func_np3, 'pointer', ['pointer', 'pointer', 'pointer']);

Interceptor.attach(set1, {
    onEnter: function (args) {
        var arg = this.context.x1;
        if (parseInt(arg) > 100000 && parseInt(arg) < 110000) { //v1
            console.log("enter functer sssssssssssssssssssss:.........")
            // console.log('func called from:\n' +
            //     Thread.backtrace(this.context, Backtracer.ACCURATE)
            //     .map(DebugSymbol.fromAddress).join('\n') + '\n');
            console.log("-----vset:");
            console.log(parseInt(arg));
            // this.context.x1 = parseInt(this.context.x1) + v1;
        }
    },
    onLeave: function (retVal) {

    }
})

Interceptor.attach(set2, {
    onEnter: function (args) {
        var arg = parseInt(this.context.x1);
        console.log("set2 arg0: " + Memory.readCString(args[0].add(10), 8))
        console.log("set2 arg1: " + arg)
        if (arg > 49000 && arg < 990500) { //v1
            console.log("enter functer set_value2:.........")
            // console.log('func called from:\n' +
            //     Thread.backtrace(this.context, Backtracer.ACCURATE)
            //     .map(DebugSymbol.fromAddress).join('\n') + '\n');
            console.log("-----vset2: " + parseInt(this.context.x1));
            // this.context.x1 = v1;
            // this.context.x1 = parseInt(this.context.x1) + v1;
        }
        if (arg < 4295953847 && arg > 4293950646) {
            console.log("-----vset2:");
            console.log('func called from:\n' +
                Thread.backtrace(this.context, Backtracer.ACCURATE)
                .map(DebugSymbol.fromAddress).join('\n') + '\n');
            this.context.x1 = v1;
            console.log("change:changechangechangechangechangechangechange: " + parseInt(this.context.x1));
        }
    },
    // onLeave: function (retVal) {
    //     console.log("leave set2" + parseInt(retVal));

    // }
})

Interceptor.attach(check_int1, {
    onEnter: function (args) {

    },
    onLeave: function (retVal) {
        console.log("-----leave check_int1:");
        // if (parseInt(retVal) == 1036 || parseInt(retVal) == 1016) { //v1
        if (parseInt(retVal) > 90000 && parseInt(retVal) < 97000) { //v1
            console.log("enter functer 111111111111111111111:.........")
            // console.log('func called from:\n' +
            //     Thread.backtrace(this.context, Backtracer.ACCURATE)
            //     .map(DebugSymbol.fromAddress).join('\n') + '\n');
            console.log("-----v2:");
            console.log(this.context.lr);
            console.log(parseInt(retVal));
            this.context.x0 = parseInt(this.context.x0) + v1;
        }
    }
})
Interceptor.attach(check_int2, {
    onEnter: function (args) {
        console.log("enter functer 22222222222222222:.........")
        console.log(parseInt(this.context.x0))
    },
    onLeave: function (retVal) {
        if (parseInt(retVal) < 4295946846 && parseInt(retVal) > 4293940746) { //v1
            console.log("leave functer 22222222222222222:.........")
            // console.log('func called from:\n' +
            //     Thread.backtrace(this.context, Backtracer.ACCURATE)
            //     .map(DebugSymbol.fromAddress).join('\n') + '\n');
            console.log("-----check_int2:");
            // console.log(this.context.lr);
            console.log(parseInt(retVal));
            // this.context.x0 = parseInt(this.context.x0) + v1;
            this.context.x0 = v1;
        }

        if (parseInt(retVal) > 40000 && parseInt(retVal) < 400000) { //v1
            console.log("leave functer no change 22222222222222222:.........")
            console.log(parseInt(retVal));
        }
    }
})