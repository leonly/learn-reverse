/*
 * @Author: Leo
 * @Date: 2021-06-29 10:11:44
 * @LastEditors: Leo
 * @LastEditTime: 2021-06-30 21:55:56
 * @Description: unity network
 */

console.log("--------------------------start---------------------------");
// var a = Process.enumerateModulesSync();
var baseaddr = Process.findModuleByName('libil2cpp.so').base;
console.log("soBaseAddr:" + baseaddr);
// UnityEngine.Networking.UnityWebRequest$$Get
var u_get = parseInt(baseaddr) + 0x16005A0; //0x23092B4
var u_get_p = new NativePointer(u_get);
var u_get_f = new NativeFunction(u_get_p, 'pointer', ['pointer']);
// var file_path = '/sdcard/info.txt';
// var file_handle = new File(file_path, "a+");
Interceptor.attach(u_get_f, {
    onEnter: function (args) {
        console.log("x0****************************************************************")
        // console.log(new NativePointer(parseInt(this.context.x1) + 32).readUtf8String());
        console.log(hexdump(this.context.x0, {
            offset: 0,
            length: 200,
            header: true,
            ansi: true
        }));
        console.log("x1****************************************************************")
        // if (file_handle && file_handle != null) {
        //     var t = new NativePointer(parseInt(this.context.x1) + 20).readUtf16String();
        //     file_handle.write(t);
        //     file_handle.write('\n\n\n');
        //     file_handle.flush();
        //     console.log("[dump]:", file_path);
        // }
        console.log(hexdump(this.context.x1, {
            offset: 0,
            length: 400,
            header: true,
            ansi: true
        }));
        // console.log(hexdump(this.context.x2, {
        //     offset: 0,
        //     length: 400,
        //     header: true,
        //     ansi: true
        // }));
        // console.log("****************************************************************")
        // console.log(hexdump(this.context.x2, {
        //     offset: 0,
        //     length: 200,
        //     header: true,
        //     ansi: true
        // }));
        console.log("****************************************************************")
        // var url = new NativePointer(parseInt(this.context.x0) + 20).readUtf16String();
        // if (url.endsWith("savefile")) {
        //     console.log("-----enter UnityWebRequest$$Get:");
        //     console.log("arg: " + url);
        //     console.log('RegisterNatives called from:\n' +
        //         Thread.backtrace(this.context, Backtracer.ACCURATE)
        //         .map(DebugSymbol.fromAddress).join('\n') + '\n');
        //     console.log('___________________________________________________________')
        // }

    },
    onLeave: function (retVal) {
        // console.log("-----leave UnityWebRequest$$Get:");
        // console.log(hexdump(retVal, {
        //     offset: 0,
        //     length: 64,
        //     header: true,
        //     ansi: true
        // }));
    }
});

var u_set_method = ptr(parseInt(baseaddr) + 0x3448CF8); //0x1E3641C
Interceptor.attach(u_set_method, {
    onEnter: function (args) {
        // var url = new NativePointer(parseInt(this.context.x1) + 20).readUtf16String();
        // v2 = a1 + 32
        // var v2 = new NativePointer(parseInt())
        var x19 = this.context.x0;

        var method = new NativePointer(new NativePointer(parseInt(x19) + 32).readU64() + 20).readUtf16String();
        if (method.toLowerCase() == 'post') {
            console.log("method: " + method);
            var url = new NativePointer(new NativePointer(parseInt(x19) + 40).readU64() + 20).readUtf16String();
            console.log("url: " + url);
            console.log("v5: " + new NativePointer(parseInt(x19) + 80).readU64());
            if (url.endsWith("savefile") || url.endsWith("track")) {
                console.log("v5 mem: " + hexdump(new NativePointer(new NativePointer(parseInt(x19) + 80).readU64() + 32), 200));
            } else {
                console.log("v5 mem: " + new NativePointer(new NativePointer(parseInt(x19) + 80).readU64() + 32).readCString());
                // console.log("v5 mem: " + new NativePointer(parseInt(x19) + 32).readUtf8String());
            }
            // if (url.endsWith("savefile")) {
            //     console.log('RegisterNatives called from:\n' +
            //         Thread.backtrace(this.context, Backtracer.ACCURATE)
            //         .map(DebugSymbol.fromAddress).join('\n') + '\n');
            // }
        }
    },
    onLeave: function (retVal) {
        // console.log("-----UnityWebRequest$$set_method:");
    }
});