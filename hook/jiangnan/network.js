/*
 * @Author: Leo
 * @Date: 2021-06-29 10:11:44
 * @LastEditors: Leo
 * @LastEditTime: 2021-06-30 16:08:45
 * @Description: unity network
 */

var count = 0;
Java.perform(function () {
    //枚举当前加载的模块
    var process_Obj_Module_Arr = Process.enumerateModules();
    for (var i = 0; i < process_Obj_Module_Arr.length; i++) {
        //包含"lib"字符串的
        if (process_Obj_Module_Arr[i].path.indexOf("link") != -1) {
            var export_funcs = process_Obj_Module_Arr[i].enumerateExports()
            for (var j = 0; j < export_funcs.length; j++) {
                if (export_funcs[j].name.indexOf("__loader_dlopen") != -1) {
                    console.log(export_funcs[j].address)
                    Interceptor.attach(export_funcs[j].address, {
                        //在hook函数之前执行的语句
                        onEnter: function (args) {
                            if (args[0].readCString().indexOf("il2cpp") != -1) {
                                console.log("-----start---------");
                                console.log(args[0].readCString());
                                count = count + 1;
                            }

                        },
                        //在hook函数之后执行的语句
                        onLeave: function (retval) {
                            if (count > 1) {
                                dumpString();
                                count = count - 1;
                            }
                        }
                    });
                }
            }
        }
    }
});


function dumpString() {
    console.log("--------------------------start---------------------------");
    // var a = Process.enumerateModulesSync();
    var baseaddr = Process.findModuleByName('libil2cpp.so').base;
    console.log("soBaseAddr:" + baseaddr);
    // UnityEngine.Networking.UnityWebRequest$$Get
    var u_get = parseInt(baseaddr) + 0x2AF572C; //0x23092B4
    var u_get_p = new NativePointer(u_get);
    var u_get_f = new NativeFunction(u_get_p, 'pointer', ['pointer']);
    // var file_path = '/sdcard/info.txt';
    // var file_handle = new File(file_path, "a+");
    Interceptor.attach(u_get_f, {
        onEnter: function (args) {
            console.log("x0****************************************************************")
            // console.log(new NativePointer(parseInt(this.context.x1) + 32).readUtf8String());
            console.log(hexdump(this.context.x0, {
                offset: 0,
                length: 200,
                header: true,
                ansi: true
            }));
            console.log("x1****************************************************************")
            // if (file_handle && file_handle != null) {
            //     var t = new NativePointer(parseInt(this.context.x1) + 20).readUtf16String();
            //     file_handle.write(t);
            //     file_handle.write('\n\n\n');
            //     file_handle.flush();
            //     console.log("[dump]:", file_path);
            // }
            // console.log(hexdump(this.context.x1, {
            //     offset: 0,
            //     length: 800,
            //     header: true,
            //     ansi: true
            // }));
            // console.log("****************************************************************")
            // console.log(hexdump(this.context.x2, {
            //     offset: 0,
            //     length: 200,
            //     header: true,
            //     ansi: true
            // }));
            console.log("****************************************************************")
            // var url = new NativePointer(parseInt(this.context.x0) + 20).readUtf16String();
            // if (url.endsWith("savefile")) {
            //     console.log("-----enter UnityWebRequest$$Get:");
            //     console.log("arg: " + url);
            //     console.log('RegisterNatives called from:\n' +
            //         Thread.backtrace(this.context, Backtracer.ACCURATE)
            //         .map(DebugSymbol.fromAddress).join('\n') + '\n');
            //     console.log('___________________________________________________________')
            // }

        },
        onLeave: function (retVal) {
            // console.log("-----leave UnityWebRequest$$Get:");
            // console.log(hexdump(retVal, {
            //     offset: 0,
            //     length: 64,
            //     header: true,
            //     ansi: true
            // }));
        }
    });

    var u_set_method = ptr(parseInt(baseaddr) + 0x1e36604); //0x1E3641C
    Interceptor.attach(u_set_method, {
        onEnter: function (args) {
            // var url = new NativePointer(parseInt(this.context.x1) + 20).readUtf16String();
            // v2 = a1 + 32
            // var v2 = new NativePointer(parseInt())
            var x19 = this.context.x0;

            var method = new NativePointer(new NativePointer(parseInt(x19) + 32).readU64() + 20).readUtf16String();
            if (method.toLowerCase() == 'post') {
                console.log("method: " + method);
                var url = new NativePointer(new NativePointer(parseInt(x19) + 40).readU64() + 20).readUtf16String();
                console.log("url: " + url);
                console.log("v5: " + new NativePointer(parseInt(x19) + 80).readU64());
                if (url.endsWith("savefile") || url.endsWith("track")) {
                    console.log("v5 mem: " + hexdump(new NativePointer(new NativePointer(parseInt(x19) + 80).readU64() + 32), 200));
                } else {
                    console.log("v5 mem: " + new NativePointer(new NativePointer(parseInt(x19) + 80).readU64() + 32).readCString());
                    // console.log("v5 mem: " + new NativePointer(parseInt(x19) + 32).readUtf8String());
                }
                // if (url.endsWith("savefile")) {
                //     console.log('RegisterNatives called from:\n' +
                //         Thread.backtrace(this.context, Backtracer.ACCURATE)
                //         .map(DebugSymbol.fromAddress).join('\n') + '\n');
                // }
            }
        },
        onLeave: function (retVal) {
            // console.log("-----UnityWebRequest$$set_method:");
        }
    });
}