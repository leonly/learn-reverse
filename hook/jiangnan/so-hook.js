/*
 * @Author: Leo
 * @Date: 2021-08-20 18:17:49
 * @LastEditors: Leo
 * @LastEditTime: 2021-08-20 18:21:13
 * @Description: 
 */
var count = 0;
Java.perform(function () {
	//枚举当前加载的模块
	var process_Obj_Module_Arr = Process.enumerateModules();
	for (var i = 0; i < process_Obj_Module_Arr.length; i++) {
		//包含"lib"字符串的
		if (process_Obj_Module_Arr[i].path.indexOf("link") != -1) {
			var export_funcs = process_Obj_Module_Arr[i].enumerateExports()
			for (var j = 0; j < export_funcs.length; j++) {
				if (export_funcs[j].name.indexOf("__loader_dlopen") != -1) {
					console.log(export_funcs[j].address)
					Interceptor.attach(export_funcs[j].address, {
						//在hook函数之前执行的语句
						onEnter: function (args) {
							if (args[0].readCString().indexOf("il2cpp") != -1) {
								console.log("-----start---------");
								console.log(args[0].readCString());
								count = count + 1;
							}

						},
						//在hook函数之后执行的语句
						onLeave: function (retval) {
							if (count > 1) {
								dumpString();
								count = count - 1;
							}
						}
					});
				}
			}
		}
	}
});


function dumpString() {
	console.log("--------------------------start---------------------------");
	// var a = Process.enumerateModulesSync();
	var baseaddr = Process.findModuleByName('libil2cpp.so').base;
	console.log("soBaseAddr:" + baseaddr);
	var x21 = parseInt(baseaddr) + 0x162D960; //0x23092B4
	var x21p = new NativePointer(x21);
	var fx21p = new NativeFunction(x21p, 'pointer', ['pointer']);
	Interceptor.attach(fx21p, {
		onEnter: function (args) {
			console.log("****************************************************************")
			// console.log(new NativePointer(parseInt(this.context.x1) + 32).readUtf8String());
			console.log(hexdump(this.context.x21, {
				offset: 0,
				length: 200,
				header: true,
				ansi: true
			}));
			console.log("****************************************************************")
		},
		onLeave: function (retVal) {
			// console.log("-----leave UnityWebRequest$$Get:");
			// console.log(hexdump(retVal, {
			//     offset: 0,
			//     length: 64,
			//     header: true,
			//     ansi: true
			// }));
		}
	});

	// var u_set_method = ptr(parseInt(baseaddr) + 0x1e36604); //0x1E3641C
	// Interceptor.attach(u_set_method, {
	// 	onEnter: function (args) {
	// 		var x19 = this.context.x0;
	// 		var method = new NativePointer(new NativePointer(parseInt(x19) + 32).readU64() + 20).readUtf16String();
	// 		if (method.toLowerCase() == 'post') {
	// 			console.log("method: " + method);
	// 			var url = new NativePointer(new NativePointer(parseInt(x19) + 40).readU64() + 20).readUtf16String();
	// 			console.log("url: " + url);
	// 			console.log("v5: " + new NativePointer(parseInt(x19) + 80).readU64());
	// 			if (url.endsWith("savefile") || url.endsWith("track")) {
	// 				console.log("v5 mem: " + hexdump(new NativePointer(new NativePointer(parseInt(x19) + 80).readU64() + 32), 200));
	// 			} else {
	// 				console.log("v5 mem: " + new NativePointer(new NativePointer(parseInt(x19) + 80).readU64() + 32).readCString());
	// 				// console.log("v5 mem: " + new NativePointer(parseInt(x19) + 32).readUtf8String());
	// 			}
	// 		}
	// 	},
	// 	onLeave: function (retVal) {
	// 		// console.log("-----UnityWebRequest$$set_method:");
	// 	}
	// });
}