/*
 * @Author: Leo
 * @Date: 2021-03-28 10:01:08
 * @LastEditors: Leo
 * @LastEditTime: 2021-06-22 17:14:14
 * @Description: 
 */
// 这个方法是为了辅助我输出用的，和python的字符串.format差不多的用法
String.prototype.format = function () {
    var values = arguments;
    return this.replace(/\{(\d+)\}/g, function (match, index) {
        if (values.length > index) {
            return values[index];
        } else {
            return "";
        }
    });
};

function buf2hex(buffer) { // buffer is an ArrayBuffer
    return Array.prototype.map.call(new Uint8Array(buffer), x => ('00' + x.toString(16)).slice(-2)).join('');
}

function hextoString(hex) {
    var arr = hex.split("")
    var out = ""
    for (var i = 0; i < arr.length / 2; i++) {
        var tmp = "0x" + arr[i * 2] + arr[i * 2 + 1]
        var charValue = String.fromCharCode(tmp);
        out += charValue
    }
    return out
}

function stringtoHex(str) {
    var val = "";
    for (var i = 0; i < str.length; i++) {
        if (val == "")
            val = str.charCodeAt(i).toString(16);
        else
            val += str.charCodeAt(i).toString(16);
    }
    val += "0a"
    return val
}

function stringToByte(str) {
    var bytes = new Array();
    var len, c;
    len = str.length;
    for (var i = 0; i < len; i++) {
        c = str.charCodeAt(i);
        if (c >= 0x010000 && c <= 0x10FFFF) {
            bytes.push(((c >> 18) & 0x07) | 0xF0);
            bytes.push(((c >> 12) & 0x3F) | 0x80);
            bytes.push(((c >> 6) & 0x3F) | 0x80);
            bytes.push((c & 0x3F) | 0x80);
        } else if (c >= 0x000800 && c <= 0x00FFFF) {
            bytes.push(((c >> 12) & 0x0F) | 0xE0);
            bytes.push(((c >> 6) & 0x3F) | 0x80);
            bytes.push((c & 0x3F) | 0x80);
        } else if (c >= 0x000080 && c <= 0x0007FF) {
            bytes.push(((c >> 6) & 0x1F) | 0xC0);
            bytes.push((c & 0x3F) | 0x80);
        } else {
            bytes.push(c & 0xFF);
        }
    }
    return bytes;


}


function byteToString(arr) {
    if (typeof arr === 'string') {
        return arr;
    }
    var str = '',
        _arr = arr;
    for (var i = 0; i < _arr.length; i++) {
        var one = _arr[i].toString(2),
            v = one.match(/^1+?(?=0)/);
        if (v && one.length == 8) {
            var bytesLength = v[0].length;
            var store = _arr[i].toString(2).slice(7 - bytesLength);
            for (var st = 1; st < bytesLength; st++) {
                store += _arr[st + i].toString(2).slice(2);
            }
            str += String.fromCharCode(parseInt(store, 2));
            i += bytesLength - 1;
        } else {
            str += String.fromCharCode(_arr[i]);
        }
    }
    return str;
}

// Resources 类hook
console.log("Script loaded successfully ");
Java.perform(function () {
    console.log("Inside java perform function");
    var purchaseClass = Java.use('com.mob.commons.authorize.a');
    console.log("Java.Use.Successfully!"); //定位类成功！
    purchaseClass.a.overload("java.util.HashMap").implementation = function (b) {
        // console.log('\n----- [start Resources.HttpReceivedData] -----');
        // console.log('param: ', a, byteToString(b), c, d, e, f);
        // console.log("params: " + a);
        var ret = this.a(b);
        console.log("resutl: " + ret);
        return false;
    }
    var purchase = Java.use('com.cmic.sso.sdk.utils.s');
    purchase.a.overload().implementation = function () {
        var ret = this.a();
        console.log("resutl2: " + ret);
        return false;
    }

    var c = Java.use('com.alipay.sdk.sys.b');
    c.b.overload().implementation = function () {
        var ret = this.b();
        console.log("resutl3: " + ret);
        return false;
    }
    var c1 = Java.use('com.alipay.security.mobile.module.b.d');
    c1.c.overload().implementation = function () {
        var ret = this.c();
        console.log("resutl4: " + ret);
        return false;
    }
    var c2 = Java.use('com.umeng.commonsdk.internal.utils.e');
    c2.a.implementation = function (b) {
        var ret = this.a(b);
        console.log("resutl5: " + ret);
        return ret;
    }
    var c3 = Java.use('com.umeng.commonsdk.internal.utils.h');
    c3.a.implementation = function () {
        var ret = this.a();
        console.log("c3-1: " + ret);
        return false;
    }
    c3.b.implementation = function () {
        var ret = this.b();
        console.log("c3-2: " + ret);
        return false;
    }
    c3.c.implementation = function () {
        var ret = this.c();
        console.log("c3-3: " + ret);
        return false;
    }
    c3.d.implementation = function () {
        var ret = this.d();
        console.log("c3-4: " + ret);
        return false;
    }
    c3.e.implementation = function () {
        var ret = this.e();
        console.log("c3-5: " + ret);
        return false;
    }
    var c4 = Java.use('com.netease.htprotect.a.d');
    c4.b.overload().implementation = function () {
        var ret = this.b();
        console.log("c4-1: " + ret);
        return false;
    }
    var cis = Java.use('com.cis.cisframework.CISNativeAndroidProtocol');
    cis.Ret_GetIsDeviceRootedRet.implementation = function (b) {
        console.log("ssssssssssssssssssssssssssssggggggggggggggggggggggggggggg")
        console.log("param: " + b);
        var ret = this.Ret_GetIsDeviceRootedRet(b);
        console.log("o: " + ret.convertToJson());
        return ret;
    }
    var c5 = Java.use('com.cis.cisframework.CISNative');
    c5.Receive_GetIsDeviceRooted.implementation = function () {
        var ret = this.Receive_GetIsDeviceRooted();
        console.log("c5-1: " + ret.convertToJson());
        return ret;
    }

    var c6 = Java.use('com.unity3d.splash.services.core.api.DeviceInfo');
    c6.isRooted.implementation = function () {
        console.log("c6-1: 0000000000000000000000");
        return;
    }
});