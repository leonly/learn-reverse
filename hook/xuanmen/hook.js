/*
 * @Author: Leo
 * @Date: 2021-04-12 22:34:46
 * @LastEditors: Leo
 * @LastEditTime: 2021-07-22 18:09:37
 * @Description: 
 */

console.log("--------------------------start---------------------------")
var a = Process.enumerateModulesSync()
var baseaddr = Process.findModuleByName('libcocos2dlua.so').base
console.log("soBaseAddr:" + baseaddr)

var v1 = 90000
var v1Num = 109


var intforkey = parseInt(baseaddr) + 0x5A5724;
var fintforkey = new NativePointer(intforkey);

var getInt = parseInt(baseaddr) + 0x5A5DBC;
var fgetInt = new NativePointer(getInt);




Interceptor.attach(fintforkey, {
    onEnter: function (args) {
        console.log("-----___________________________enter intforkey *********** page:");
        console.log("x1 : " + hexdump(this.context.x1));
    },
    onLeave: function (retVal) {
        // if (parseInt(retVal) > 4294856866 && parseInt(retVal) < 4294956866) {
        console.log("-----___________________________leave intforkey *********** page:");
        console.log("x0: " + parseInt(this.context.x0));
    }
});

Interceptor.attach(fgetInt, {
    onEnter: function (args) {
        console.log("-----___________________________enter getint *********** page:");
        console.log("x1 : " + hexdump(this.context.x1));
    },
    onLeave: function (retVal) {
        // if (parseInt(retVal) > 4294856866 && parseInt(retVal) < 4294956866) {
        console.log("-----___________________________leave getint *********** page:");
        console.log("x0: " + parseInt(this.context.x0));
    }

})