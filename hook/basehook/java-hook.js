/*
 * @Descripttion: 远程调用,这一小节我们要实现的是，不仅要在跑在安卓机上的js脚本里调用这个函数，还要可以在kali主机上的py脚本里，直接调用这个函数。也就是使用frida提供的RPC功能（Remote Procedure Call）
 * @version: 
 * @Author: Leo
 * @Last Modified by: Leo
 * @Last Modified time: Do not Edit
 */
console.log("Script loaded successfully ");

function callSecretFun() { //定义导出函数
    Java.perform(function () { //找到隐藏函数并且调用
        Java.choose("com.cis.jiangnan.taptap", {
            onMatch: function (instance) {
                console.log("Found instance: " + instance);
                console.log("Result of secret func: " + instance.secret());
            },
            onComplete: function () { }
        });
    });
}
rpc.exports = {
    callsecretfunction: callSecretFun //把callSecretFun函数导出为callsecretfunction符号，导出名不可以有大写字母或者下划线
};