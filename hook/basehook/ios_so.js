/*
 * @Descripttion: 
 * @version: 
 * @Author: Leo
 * @Date: 2021-01-16 15:06:18
 * @LastEditor: Leo
 * @LastEditTime: 2021-01-30 11:40:39
 */

function getNSHomeDirectory() {
    if (ObjC.available) {
        var NSHomeDirectory = new NativeFunction(ptr(Module.findExportByName("Foundation", "NSHomeDirectory")), 'pointer', []);
        var path = new ObjC.Object(NSHomeDirectory());
        console.log(path);
    }
}


function hook_native() {
    if (ObjC.available) {
        var NSHomeDirectory = new NativeFunction(ptr(Module.findExportByName("Foundation", "NSHomeDirectory")), 'pointer', []);
        var path = new ObjC.Object(NSHomeDirectory());
        console.log(path);
    }
    var baseAddress = Module.findBaseAddress("Sky-iOS-Gold");
    console.log("Sky-iOS-Gold: " + baseAddress);
    // var nativePointer = new NativePointer(baseAddress); 
    // getprivatekey
    Interceptor.attach(baseAddress.add(0x12af818), {
        onEnter: function (args) {
            console.log("enter sec key rao sign function....");
            // console.log('func called from:\n' +
            //     Thread.backtrace(this.context, Backtracer.ACCURATE)
            //     .map(DebugSymbol.fromAddress).join('\n') + '\n');
            console.log("x0: " + this.context.x0);
            console.log("x0_byte: " + hexdump(Memory.readByteArray(this.context.x0, 200)));
            console.log("x1: " + this.context.x1);
            console.log("x1_byte: " + hexdump(Memory.readByteArray(this.context.x1, 200)));
            console.log("x2: " + this.context.x2);
            console.log("x2_byte: " + hexdump(Memory.readByteArray(this.context.x2, 200)));
            // console.log("w3: " + this.context.w3);
            // console.log("x3_byte: " + Memory.readU32(this.context.w3));
            // console.log("x4: " + this.context.x4);
            // console.log("x4_byte: " + hexdump(Memory.readByteArray(this.context.x4, 200)));
            // console.log("x5: " + this.context.x5);
            // console.log("x5_byte: " + hexdump(Memory.readByteArray(this.context.x5, 200)));
            // console.log("x6: " + this.context.x6);
            // console.log("x6_byte: " + hexdump(Memory.readByteArray(this.context.x6, 200)));
        },
        onLeave: function (retval) {
            console.log("===================OnLeave=================");
            console.log("retval:", retval);
        }
    });

    // signwithdevicekey
    Interceptor.attach(baseAddress.add(0x05b220), {
        onEnter: function (args) {
            console.log("enter sign_with device key function....");
            // console.log('uuid called from:\n' +
            //     Thread.backtrace(this.context, Backtracer.ACCURATE)
            //     .map(DebugSymbol.fromAddress).join('\n') + '\n');
            console.log("x0: " + this.context.x0);
            console.log("x0_byte: " + hexdump(Memory.readByteArray(this.context.x0, 4)));
            console.log("x1: " + this.context.x1);
            console.log("x1_byte: " + hexdump(Memory.readByteArray(this.context.x1, 100)));
            // console.log("x2: " + this.context.x2);
            // console.log("x2_byte: " + hexdump(Memory.readByteArray(this.context.x2, 200)));
            // console.log("x3: " + this.context.x3);
            // console.log("x3_byte: " + hexdump(Memory.readByteArray(this.context.x3, 200)));
            // console.log("x4: " + this.context.x4);
        },
        onLeave: function (retval) {
            // retval.replace(ptr(0));
            console.log("===================OnLeave=================");
            console.log("retval:", retval);
        }
    });

    // accountbarn::setPushNotificationDeviceToken
    // Interceptor.attach(baseAddress.add(0x078dcb0), {

    // Interceptor.attach(baseAddress.add(0x7ba718), {
    //     onEnter: function (args) {
    //         console.log("enter function....");
    //         console.log('uuid called from:\n' +
    //             Thread.backtrace(this.context, Backtracer.ACCURATE)
    //             .map(DebugSymbol.fromAddress).join('\n') + '\n');
    //         console.log("x0: " + this.context.x0);
    //         console.log("x1: " + this.context.x1);
    //         console.log("x2: " + this.context.x2);
    //         console.log("x3: " + this.context.x3);
    //         console.log("x4: " + this.context.x4);
    //         console.log("x5: " + this.context.x5);
    //         console.log("accountServerClient: " + hexdump(Memory.readByteArray(this.context.x0, 200)));
    //         console.log("sig: " + hexdump(Memory.readByteArray(Memory.readByteArray(this.context.x0, 4), 4)));
    //     }
    // });

}

setImmediate(hook_native());