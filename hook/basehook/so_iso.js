/*
 * @Descripttion: 
 * @version: 
 * @Author: Leo
 * @Date: 2021-01-16 15:06:18
 * @LastEditor: Leo
 * @LastEditTime: 2021-01-17 15:38:03
 */
function hook_native() {
    var baseAddress = Module.findBaseAddress("Sky-iOS-Gold");
    console.log("Sky-iOS-Gold: " + baseAddress);
    // var nativePointer = new NativePointer(baseAddress);
    Interceptor.attach(baseAddress.add(0x12af7e8), {
        onEnter: function (args) {
            console.log("enter seckeyrawsign function....");
            console.log('func called from:\n' +
                Thread.backtrace(this.context, Backtracer.ACCURATE)
                .map(DebugSymbol.fromAddress).join('\n') + '\n');
            console.log("x0: " + this.context.x0);
            console.log("x0_byte: " + hexdump(Memory.readByteArray(this.context.x0, 200)));
            console.log("x1: " + this.context.x1);
            console.log("x1_byte: " + hexdump(Memory.readByteArray(this.context.x1, 200)));
            console.log("x2: " + this.context.x2);
            console.log("x2_byte: " + hexdump(Memory.readByteArray(this.context.x2, 200)));
            console.log("x3: " + this.context.x3);
            console.log("x3_byte: " + hexdump(Memory.readByteArray(this.context.x3, 200)));
            console.log("x4: " + this.context.x4);
            console.log("x4_byte: " + hexdump(Memory.readByteArray(this.context.x4, 200)));
            console.log("x5: " + this.context.x5);
            console.log("x5_byte: " + hexdump(Memory.readByteArray(this.context.x5, 200)));
            console.log("x6: " + this.context.x6);
            console.log("x6_byte: " + hexdump(Memory.readByteArray(this.context.x6, 200)));
            // console.log("arg[0]: " + hexdump(Memory.readByteArray(args[0], 300), {
            //     offset: 0,
            //     length: 300,
            //     header: true,
            //     ansi: false
            // }));
            // console.log("device_name: " + Memory.readCString(args[0].add(10), 16));
            // console.log("tos_version: " + Memory.readU32(args[0].add(88)));
            // console.log("sig_recover: " + Memory.readCString(args[0].add(229), 5));
        },
        onLeave: function (retval) {
            console.log("==========================leave=======================")
            console.log("retval:", retval);
        }
    });

    // accountbarn::setPushNotificationDeviceToken
    // Interceptor.attach(baseAddress.add(0x078dcb0), {

    // Interceptor.attach(baseAddress.add(0x7ba718), {
    //     onEnter: function (args) {
    //         console.log("enter function....");
    //         console.log('uuid called from:\n' +
    //             Thread.backtrace(this.context, Backtracer.ACCURATE)
    //             .map(DebugSymbol.fromAddress).join('\n') + '\n');
    //         console.log("x0: " + this.context.x0);
    //         console.log("x1: " + this.context.x1);
    //         console.log("x2: " + this.context.x2);
    //         console.log("x3: " + this.context.x3);
    //         console.log("x4: " + this.context.x4);
    //         console.log("x5: " + this.context.x5);
    //         console.log("accountServerClient: " + hexdump(Memory.readByteArray(this.context.x0, 200)));
    //         console.log("sig: " + hexdump(Memory.readByteArray(Memory.readByteArray(this.context.x0, 4), 4)));
    //     }
    // });

}
setImmediate(hook_native());