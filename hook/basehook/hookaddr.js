/*
 * @Descripttion: 
 * @version: 
 * @Author: Leo
 * @Date: 2021-01-16 19:26:11
 * @LastEditor: Leo
 * @LastEditTime: 2021-01-16 19:26:26
 */
var baseAddr_0 = Module.findBaseAddress('libBootloader.so');
console.log(baseAddr_0)
var luaL_loadbuffer_0 = baseAddr_0.add(0x07beab4);

Interceptor.attach(luaL_loadbuffer_0,{
	onEnter:function(args){
       try{
			console.log('Backtrace:	' + Thread.backtrace(this.context,Backtracer.ACCURATE).map(DebugSymbol.fromAddress).join('')); 
			
			console.log('参数打印 param0:', args[0]);
			
			console.log('参数打印 param1:', args[1]);
			
			console.log('参数打印 param2:', args[2]);
 
			 
        }catch(e){
            console.log(e);
        }
	},
	onLeave:function(retval){
       try{
            console.log('函数执行结果：' + retval); 
             
       }catch(e){
            console.log(e);
       }
	}
});