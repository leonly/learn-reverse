var count = 0;
Java.perform(function () {
    //枚举当前加载的模块
    var process_Obj_Module_Arr = Process.enumerateModules();
    for (var i = 0; i < process_Obj_Module_Arr.length; i++) {
        //包含"lib"字符串的
        if (process_Obj_Module_Arr[i].path.indexOf("link") != -1) {
            var export_funcs = process_Obj_Module_Arr[i].enumerateExports()
            for (var j = 0; j < export_funcs.length; j++) {
                if (export_funcs[j].name.indexOf("__loader_dlopen") != -1) {
                    console.log(export_funcs[j].address)
                    Interceptor.attach(export_funcs[j].address, {
                        //在hook函数之前执行的语句
                        onEnter: function (args) {
                            if (args[0].readCString().indexOf("libmonobdwgc") != -1) {
                                console.log("-----start---------");
                                console.log(args[0].readCString());
                                count = count + 1;
                            }

                        },
                        //在hook函数之后执行的语句
                        onLeave: function (retval) {
                            if (count > 1) {
                                dumpString();
                                count = count - 1;
                            }
                        }
                    });
                }
            }
        }
    }
});


function dumpString() {
    console.log("--------------------------start---------------------------");
    var baseaddr = Process.findModuleByName('libmonobdwgc-2.0.so').base;
    console.log("soBaseAddr:" + baseaddr);
    var do_mono_image_load = ptr(parseInt(baseaddr) + 0x2F5F90);
    Interceptor.attach(do_mono_image_load, {
        onEnter: function (args) {
            console.log("r0: " + this.context.r0);
            console.log("r1: " + this.context.r1);
        },
        onLeave: function (retVal) {
            console.log("retVal: " + retval);
        }
    });
}