/*
 * @Author: Leo
 * @Date: 2021-03-28 10:01:08
 * @LastEditors: Leo
 * @LastEditTime: 2021-04-11 16:55:26
 * @Description: 
 */
setTimeout(function () {
    Java.perform(function () {
        console.log("\n[*] enumerating classes...");
        Java.enumerateLoadedClasses({
            onMatch: function (_className) {
                if (_className.split(".")[1] == "bluetooth") {
                    console.log("[->]\t"+_className);
                }
            },
            onComplete: function () {
                console.log("[*] class enuemration complete");
            }
        });
    });
});