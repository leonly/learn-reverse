'''
Descripttion: 
version: 
Author: Leo
Date: 1970-01-01 08:00:00
Last Modified by: Leo
Last Modified time: Do not Edit
'''
import time
import frida


# 定义错误处理
def my_message_handler(message, payload):
    print(message)
    print(payload)


device = frida.get_usb_device()
pid = device.spawn(["com.cis.jiangnan.taptap"])
device.resume(pid)
time.sleep(4)
session = device.attach(pid)
with open("so.js", "r", encoding="utf8") as f:
    script = session.create_script(f.read())
script.on("message", my_message_handler)
script.load()

input()
