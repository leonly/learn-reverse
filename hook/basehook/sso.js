/*
 * @Descripttion: 
 * @version: 
 * @Author: Leo
 * @Date: 1970-01-01 08:00:00
 * @Last Modified by: Leo
 * @Last Modified time: Do not Edit
 */

function hook_native() {
    // console.log(JSON.stringify(Process.enumerateModules()));
    var libnative_addr = Module.findBaseAddress("libSrc.so");
    console.log("libSrc: " + libnative_addr);

    if (libnative_addr) {
        var string_with_jni_addr = Module.findExportByName("libSrc.so", 
        "Java_com_ea_EAIO_EAIO_StartupNativeImpl");
        console.log("string_with_jni_addr is: " + string_with_jni_addr);
        console.log("relative address: ",  (string_with_jni_addr - libnative_addr).toString(16));
    }

    Interceptor.attach(Module.findExportByName("libSrc.so", "Java_com_ea_EAIO_EAIO_StartupNativeImpl"), {
        onEnter: function (args) {
            console.log("**************start on enter**************")
            var name = this.context.x0.readCString();
            console.log("enter function. : ", this.returnAddress, name);
            console.log('RegisterNatives called from: \n' + Thread.backtrace(this.context, Backtracer.ACCURATE).map(DebugSymbol.fromAddress).join('\n') + '\n')
            // console.log("string_with_jni args: " + args[0], args[1], args[2], args[3], args[4], args[5]);
            // console.log(Java.vm.getEnv().getStringUtfChars(args[0], null).readCString());
            // console.log(Java.vm.getEnv().getStringUtfChars(args[1], null).readCString());
            // console.log(Java.vm.getEnv().getStringUtfChars(args[2], null).readCString());
            // console.log(Java.vm.getEnv().getStringUtfChars(args[3], null).readCString());
            // console.log(Java.vm.getEnv().getStringUtfChars(args[4], null).readCString());
            // console.log(Java.vm.getEnv().getStringUtfChars(args[5], null).readCString());
        },
        onLeave: function (retval) {
            console.log("retval:", retval);
            // console.log(Java.vm.getEnv().getStringUtfChars(retval, null).readCString())
            // var newRetval = Java.vm.getEnv().newStringUtf("new retval from hook_native");
            // retval.replace(ptr(newRetval));
        }
    });
}

function hook_libc(){
    var pthread_create_addr = null;
    var symbols = Process.findModuleByName("libSrc.so").enumerateSymbols();
    for(var i = 0;i<symbols.length;i++){
        var symbol = symbols[i].name;
        if(symbol.indexOf("pthread_create")>=0){
            //console.log(symbols[i].name);
            //console.log(symbols[i].address);
            pthread_create_addr = symbols[i].address;
        }
    }
    console.log("pthread_create_addr,",pthread_create_addr);
    Interceptor.attach(pthread_create_addr,{
        onEnter:function(args){
            console.log("pthread_create_addr args[0],args[1],args[2],args[3]:",args[0],args[1],args[2],args[3]);

        },onLeave:function(retval){
            console.log("retval is:",retval)
        }
    })
}

function inline_hook() {
    var libnative_lib_addr = Module.findBaseAddress("libBootloader.so");
    if (libnative_lib_addr) {
        // var so_method = Module.findExportByName("libSrc.so", "sub_1DFB770");
        console.log("libnative_lib_addr:", libnative_lib_addr);
        // console.log("addr:", libnative_lib_addr - 0x1dfb770);
        var addr_775C = libnative_lib_addr.add(0x1dfb770);
        console.log("addr_0x1dfb770:", addr_775C);
        // var so_method = new NativeFunction(addr_775C, "int", ["int", "int", "pointer"]);
        Java.perform(function () {
            Interceptor.attach(addr_775C, {
                onEnter: function (args) {
                    // log heap stack
                    console.log('CCCryptorCreate called from:\n' + Thread.backtrace(this.context, Backtracer.ACCURATE).map(DebugSymbol.fromAddress).join('\n') + '\n');
                    var name = this.context.r0;
                    console.log("name r0:", name, this.context.r1);
                    console.log("string_with_jni args: " + args[0], args[1], args[2], args[3]);
                },
                onLeave: function (retval) {
                     console.log("retval is :", retval) 
                }
            })
        })
    }
}

// setImmediate(hook_native());
setImmediate(inline_hook());
// Java.perform(function () {
//     var so_method = new NativeFunction('0x1DFB770', 'int', ['int', 'int', 'pointer']);
//     // var so_method = new NativeFunction(Module.findExportByName('libSrc.so', 'sub_1DFB770'),
//     //     'int', ['int', 'int', 'pointer']);
//     // console.log("result:", so_method(1, 2));
//     // 在这里new一个新的函数，但是参数的个数和返回值必须对应
//     Interceptor.replace(add_method, new NativeCallback(function (a, b) {
//         return 123;
//     }, 'int', ['int', 'int']));
//     console.log("result:", add_method(1, 2));
// });