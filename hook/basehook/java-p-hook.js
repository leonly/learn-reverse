/*
 * @Descripttion:js代码的逻辑就是，截取输入，传输给kali主机，暂停执行，得到kali主机传回的数据之后，继续执行
 * @version: 
 * @Author: Leo
 * @Date: 1970-01-01 08:00:00
 * @Last Modified by: Leo
 * @Last Modified time: Do not Edit
 */

console.log("Script loaded successfully ");
Java.perform(function () {
    var tv_class = Java.use("android.widget.TextView");
    tv_class.setText.overload("java.lang.CharSequence").implementation = function (x) {
        var string_to_send = x.toString();
        var string_to_recv;
        send(string_to_send); // 将数据发送给kali主机的python代码
        recv(function (received_json_object) {
            string_to_recv = received_json_object.my_data
            console.log("string_to_recv: " + string_to_recv);
        }).wait(); //收到数据之后，再执行下去
        return this.setText(string_to_recv);
    }
});