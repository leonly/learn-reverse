* 1. burpsuit代理 *
1. 设置好 burp软件透明代理
2. 在burp客户端进行证书导出，然后复制到手机并修改权限755
3. > adb shell
4. > su
5. > cp /data/misc/user/0/cacerts-added/* /system/etc/security/cacerts/
6. > chmod 755 /system/etc/security/cacerts/*
7. > iptables -t nat -A OUTPUT -p tcp --dport 443 -j REDIRECT --to-ports 8899
8. > iptables -t nat -A OUTPUT -p tcp --dport 80 -j REDIRECT --to-ports 8899 
9. > exit, exit
10. > adb reverse tcp:8899 tcp:8899
11. > 取消客户端透明代理
12. > adb shell & su
13. > iptables -t nat -D OUTPUT -p tcp --dport 443 -j REDIRECT --to-ports 8899
14. > 移除端口映射
15. > adb reverse --remove-all
