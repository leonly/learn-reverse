/*
 * @Descripttion: 
 * @version: 
 * @Author: Leo
 * @Date: 1970-01-01 08:00:00
 * @Last Modified by: Leo
 * @Last Modified time: Do not Edit
 */

#include "bstr.h"

using namespace std;

void append_test()
{
    // The first member function
    // appending a C-string to a string
    string str1a("Hello ");
    const char *cstr1a = "Out There ";
    str1a.append(cstr1a);
    // The second member function
    // appending part of a C-string to a string
    string str1b("Hello ");
    const char *cstr1b = "Out There ";
    str1b.append(cstr1b, 3);

    // The third member function
    // appending part of one string to another
    string str1c("Hello "), str2c("Wide World ");
    str1c.append(str2c, 5, 5);
    // The fourth member function
    // appending one string to another in two ways,
    // comparing append and operator [ ]
    string str1d("Hello "), str2d("Wide "), str3d("World ");
    // printf("The  string str2d is: " str2d );
    str1d.append(str2d);
    str1d += str3d;
    // The fifth member function
    // appending characters to a string
    string str1e("Hello ");
    str1e.append(4, '!');
    // The sixth member function
    // appending a range of one string to another
    string str1f("Hello "), str2f("Wide World ");
    str1f.append(str2f.begin() + 5, str2f.end() - 1);
    printf("The string str2f is: %s", str2f.c_str());
}

void assign_test()
{
    // The first member function assigning the
    // characters of a C-string to a string
    string str1a;
    const char *cstr1a = "Out There";
    str1a.assign(cstr1a);
    // The second member function assigning a specific
    // number of the of characters a C-string to a string
    string str1b;
    const char *cstr1b = "Out There";
    // printf("The C-string cstr1b is: " cstr1b );
    printf("The C-string cstr1b is: %s", cstr1b);
    str1b.assign(cstr1b, 3);
    printf("The C-string cstr1b now is: %s", cstr1b);

    // The third member function assigning a specific number
    // of the characters from one string to another string
    string str1c("Hello "), str2c("Wide World ");
    // printf("The string str2c is: " str2c );
    printf("The string str2c is: %s", str2c.c_str());
    str1c.assign(str2c, 5, 5);
    // The fourth member function assigning the characters
    // from one string to another string in two equivalent
    // ways, comparing the assign and operator =
    string str1d("Hello"), str2d("Wide"), str3d("World");
    str1d.assign(str2d);
    str1d = str3d;
    // The fifth member function assigning a specific
    // number of characters of a certain value to a string
    string str1e("Hello ");
    str1e.assign(4, '!');
    // The sixth member function assigning the value from
    // the range of one string to another string
    string str1f("Hello "), str2f("Wide World ");
    str1f.assign(str2f.begin() + 5, str2f.end() - 1);
}

void at_test()
{
    string str1("Hello world"), str2("Goodbye world");
    const string cstr1("Hello there"), cstr2("Goodbye now");
    // Element access to the non const strings
    basic_string<char>::reference refStr1 = str1[6];
    basic_string<char>::reference refStr2 = str2.at(3);

    // Element access to the const strings
    basic_string<char>::const_reference crefStr1 = cstr1[cstr1.length()];
    basic_string<char>::const_reference crefStr2 = cstr2.at(8);
    printf("The char2 is: %c", crefStr2);
    printf("The char1 is: %c", crefStr1);
}

void constructor_test()
{
    // The first member function initializing with a C-string
    const char *cstr1a = "Hello Out There.";
    basic_string<char> str1a(cstr1a, 5);
    // The second member function initializing with a string
    string str2a("How Do You Do");
    basic_string<char> str2b(str2a, 7, 7);
    // The third member function initializing a string
    // with a number of characters of a specific value
    basic_string<char> str3a(5, '9');
    // The fourth member function creates an empty string
    // and string with a specified allocator
    basic_string<char> str4a;
    string str4b;
    basic_string<char> str4c(str4b.get_allocator());
    if (str4c.empty())
        printf("The string str4c is empty.");
    else
        printf("The string str4c is not empty.");

    // The fifth member function initializes a string from
    // another range of characters
    string str5a("Hello World");
    basic_string<char> str5b(str5a.begin() + 5, str5a.end());
}

void begin_test()
{
    string str1("No way out."), str2;
    basic_string<char>::iterator strp_Iter, str1_Iter, str2_Iter;
    basic_string<char>::const_iterator str1_cIter;

    str1_Iter = str1.begin();
    printf("The first character of the string str1 is: %c", *str1_Iter);
    printf("The full original string str1 is: %s", str1.c_str());

    // The dereferenced iterator can be used to modify a character
    *str1_Iter = 'G';
    printf("The first character of the modified str1 is now: %c", *str1_Iter);
    char at2 = *++str1_Iter;
    printf("The next modified string str1 is now: %c", at2);
    printf("The full modified string str1 is now: %s", str1.c_str());

    // The following line would be an error because iterator is const
    // *str1_cIter = 'g';

    // For an empty string, begin is equivalent to end
    if (str2.begin() == str2.end())
        printf("The string str2 is empty.");
    else
        printf("The string str2 is not empty.");
}

void cstr_test()
{
    string str1("Hello world");
    const char *c_str1 = str1.c_str();
    printf("The cstr is: %s", c_str1);
}

void size_test()
{
    string str1("Hello world");
    basic_string<char>::size_type sizeStr1, lenStr1, capStr1, max_sizeStr1;
    sizeStr1 = str1.size();
    lenStr1 = str1.length();
    capStr1 = str1.capacity();
    max_sizeStr1 = str1.max_size();
    printf("The original string object str1 is: %s", str1.c_str());
    printf("The current size of modified string str1 is: %lu", sizeStr1);
    printf("The current length of modified string str1 is: %lu", lenStr1);
    printf("The capacity of string str1 is: %lu", capStr1);
    printf("The max_size of string str1 is: %lu", max_sizeStr1);

    // Converting a string to an array of characters
    const char *ptr1 = 0;
    ptr1 = str1.data();
    printf("The modified string object ptr1 is: %s", ptr1);
}

void clear_test()
{
    string str1("Hello world");
    str1.clear();
    str1.insert(0, "a");
    printf("%s", str1.c_str());
    str1.append(" hello");
    printf("%s", str1.c_str());
    str1.erase(1, 1);
    printf("%s", str1.c_str());
}

void compare_test()
{
    // The first member function compares
    // an operand string to a parameter string
    int comp1;
    string s1o("CAB");
    string s1p("CAB");
    printf("The operand string is: %s", s1o.c_str());
    printf("The parameter string is: %s", s1p.c_str());
    comp1 = s1o.compare(s1p.c_str());
    if (comp1 < 0)
        printf("The operand string is less than "
               "the parameter string.");
    else if (comp1 == 0)
        printf("The operand string is equal to "
               "the parameter string.");
    else
        printf("The operand string is greater than "
               "the parameter string.");
}

void copy_test()
{
    string str1("Hello World");
    basic_string<char>::iterator str_Iter;
    // char array1[20] = {0};
    char array2[10] = {0};
    // basic_string<char>::pointer array1Ptr = array1;
    basic_string<char>::value_type *array2Ptr = array2;

    printf("The original string str1 is: ");
    for (str_Iter = str1.begin(); str_Iter != str1.end(); str_Iter++)
        printf("%c ", *str_Iter);
    printf("end");
    // basic_string<char>::size_type nArray1;
    basic_string<char>::size_type nArray2;
    // Note: string::copy is potentially unsafe, consider
    // using string::_Copy_s instead.
    // nArray1 = str1._Copy_s(array1Ptr, 20, 12);
    nArray2 = str1.copy(array2Ptr, 5, 6); // C4996
    // printf("The copied characters array1 is: " array1Ptr );
    printf("The copied characters array2 is: %s", array2Ptr);
}

void some_op_test()
{
    string str1("Hello world, toiny jim!");
    printf("The length of the string object str1 = %lu", str1.length());

    // Converting a string to an array of characters
    const char *ptr1 = 0;
    ptr1 = str1.data();
    printf("The modified string object ptr1 is: %s", ptr1);
    basic_string<char>::size_type indexChFi, indexChLi;
    indexChFi = str1.find_first_of("i");
    indexChLi = str1.find_last_of("i");
    basic_string<char>::difference_type diffi = indexChLi - indexChFi;

    printf("The first character i is at position: %lu", indexChFi);
    printf("The last character i is at position: %lu", indexChLi);
    printf("The difference is: %ld", diffi);
    basic_string<char>::iterator str1_Iter;
    str1_Iter = str1.end();
    str1_Iter--;
    str1_Iter--;
    bool b2 = str1.empty();
    if (b2)
        printf("The string object str1 is empty.");
    else
        printf("The string object str1 is not empty.");
}

void find_test()
{
    // The first member function
    // searches for a single character in a string
    string str1("Hello Everyone");
    printf("The original string str1 is: %s", str1.c_str());
    basic_string<char>::size_type indexCh1a, indexCh1b;

    indexCh1a = str1.find("e", 3);
    if (indexCh1a != string::npos)
        printf("The index of the 1st 'e' found after the 3rd position in str1 is: %lu", indexCh1a);
    else
        printf("The character 'e' was not found in str1 .");

    indexCh1b = str1.find("x");
    if (indexCh1b != string::npos)
        printf("The index of the 'x' found in str1 is: %lu", indexCh1b);
    else
        printf("The Character 'x' was not found in str1.");

    str1.assign("xddd-1234-abcd");
    printf("The original string str1 is: %s", str1.c_str());
    static const basic_string<char>::size_type npos = -1;

    indexCh1a = str1.find_first_not_of("d", 2);
    if (indexCh1a != npos)
        printf("The index of the 1st 'd' found after the 3rd position in str1 is: %lu", indexCh1a);
    else
        printf("The character 'd' was not found in str1 .");
    indexCh1b = str1.find_first_not_of("x");
    if (indexCh1b != npos)
        printf("The index of the 'non x' found in str1 is: %lu", indexCh1b);
    else
        printf("The character 'non x' was not found in str1.");
    indexCh1a = str1.find_first_of("d", 5);
    if (indexCh1a != npos)
        printf("The index of the 1st 'd' found after the 5th position in str1 is: %lu", indexCh1a);
    else
        printf("The character 'd' was not found in str1 .");

    indexCh1b = str1.find_first_of("x");
    if (indexCh1b != npos)
        printf("The index of the 'x' found in str1 is: %lu", indexCh1b);
    else
        printf("The character 'x' was not found in str1.");
    indexCh1a = str1.find_last_not_of("d", 7);
    if (indexCh1a != npos)
        printf("The index of the last non 'd'\n found before the 7th position in str1 is: %lu", indexCh1a);
    else
        printf("The non 'd' character was not found .");

    indexCh1b = str1.find_last_not_of("d");
    if (indexCh1b != npos)
        printf("The index of the non 'd' found in str1 is: %lu", indexCh1b);
    else
        printf("The Character 'non x' was not found in str1.");
    indexCh1a = str1.find_last_of("d", 14);
    if (indexCh1a != npos)
        printf("The index of the last 'd' found before the 14th position in str1 is: %lu", indexCh1a);
    else
        printf("The character 'd' was not found in str1 .");

    indexCh1b = str1.find_first_of("x");
    if (indexCh1b != npos)
        printf("The index of the 'x' found in str1 is: %lu", indexCh1b);
    else
        printf("The character 'x' was not found in str1.");
}

void cop_test()
{
    string str1a("Hello");
    str1a += '!';
    printf("The string str1 appended with an exclamation is: %s", str1a.c_str());
    // The second member function
    // appending a C-string to a string
    string str1b("Hello ");
    const char *cstr1b = "Out There";
    str1b += cstr1b;
    printf("Appending the C-string cstr1b to string str1 gives: %s", str1b.c_str());
    str1b = cstr1b;
    printf("The C-string cstr1b is: %s", str1b.c_str());
    str1b.pop_back();
    printf("%s", str1b.c_str());
    str1b.push_back('Y');
    printf("%s", str1b.c_str());
}

void reference_test()
{
    string str1("Able was I ere I saw Elba");
    basic_string<char>::reverse_iterator str_rIter, str_rIterEnd;
    for (str_rIter = str1.rbegin(); str_rIter != str1.rend(); str_rIter++)
        printf("%c ", *str_rIter);
    for (str_rIterEnd = str1.rend(); str_rIter != str1.rbegin() - 1; str_rIter--)
        printf("%c ", *str_rIter);
}

int main(int argc, char const *argv[])
{
    append_test();
    assign_test();
    at_test();
    constructor_test();
    begin_test();
    cstr_test();
    size_test();
    clear_test();
    compare_test();
    copy_test();
    some_op_test();
    find_test();
    cop_test();
    reference_test();
    return 0;
}
