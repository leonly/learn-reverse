/*
 * @Descripttion: 
 * @version: 
 * @Author: Leo
 * @Date: 1970-01-01 08:00:00
 * @Last Modified by: Leo
 * @Last Modified time: Do not Edit
 */
#include <string>

void append_test();
void assign_test();
void at_test();
void constructor_test();
void begin_test();
void cstr_test();
void size_test();
void clear_test();
void compare_test();
void copy_test();
void some_op_test();
void find_test();
void cop_test();
void reference_test();