/*
 * @Descripttion: 
 * @version: 
 * @Author: Leo
 * @Date: 1970-01-01 08:00:00
 * @Last Modified by: Leo
 * @Last Modified time: Do not Edit
 */
#ifndef _RC4_H_
#define _RC4_H_

using std::string;
class RC4
{
private:
    /* data */
    unsigned char S[256]; //临时向量
    unsigned char T[256]; //状态向量
    const char *keystr;
    int keylen;
    void init(const char *key);

public:
    RC4(const string &key);
    string getKeystr() {return keystr;}
    string rc4(const string &text);
    static char* char_2_Hex(const char *Char);
    //16 -> char
    static char* Hex_2_char(const char *Char);
    ~RC4();
};

#endif