/*
 * @Descripttion: 
 * @version: 
 * @Author: Leo
 * @Date: 1970-01-01 08:00:00
 * @Last Modified by: Leo
 * @Last Modified time: Do not Edit
 */
//
// Created by river on 11/25/20.
//

#ifndef _BASE64_H_
#define _BASE64_H_

using std::string;

static const string base64_chars =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz"
        "0123456789+/";
class Base64
{
private:
public:
    // Base64(/* args */);
    static string encode(const string&);
    static string decode(const string&);
    static string base64_encode(uint8_t const* buf, unsigned int bufLen);
    // ~Base64();
};
#endif //ALGORITHOM_BASE64_H
