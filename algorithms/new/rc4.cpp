/*
 * @Descripttion: 
 * @version: 
 * @Author: Leo
 * @Date: 1970-01-01 08:00:00
 * @Last Modified by: Leo
 * @Last Modified time: Do not Edit
 */
#include <iostream>
#include <string.h>
#include "rc4.h"
using namespace std;

RC4::RC4(const string &key) : keylen(strlen(key.c_str()))
{
    keystr = key.c_str();
}

void RC4::init(const char *key)
    {
        for (int i = 0; i < 256; i++)
        {
            S[i] = i;
            int tmp = i % keylen;
            T[i] = key[tmp];
        }
        int j = 0;
        for (int i = 0; i < 256; i++)
        {
            j = (j + S[i] + T[i]) % 256;
            int tmp;
            tmp = S[j];
            S[j] = S[i];
            S[i] = tmp;
        }
    }

char* RC4::char_2_Hex(const char *Char)
{
    int length = strlen(Char);
    char *Hex = new char[2 * length];
    for (size_t i = 0; i < length; i++)
    {
        int tmp = int(Char[i]);
        if (Char[i] < 0)
            tmp = (-1) * Char[i] + 128;
        int high = tmp / 16;
        int low = tmp % 16;
        char HIHG;
        char LOW;

        if (high >= 10)
            HIHG = char(high - 10 + 65);
        else
            HIHG = char(high + 48);

        if (low >= 10)
            LOW = char(low - 10 + 65);
        else
            LOW = char(low + 48);

        Hex[2 * i] = HIHG;
        Hex[2 * i + 1] = LOW;
    }
    return Hex;
}

//16 -> char
char* RC4::Hex_2_char(const char *Hex)
{
    int length = strlen(Hex) / 2;
    char *Char = new char[length];
    for (size_t i = 0; i < length; i++)
    {
        int high;
        int low;
        if (int(Hex[2 * i]) >= 65)
            high = int(Hex[2 * i] - 65 + 10);
        else
            high = int(Hex[2 * i] - 48);

        if (int(Hex[2 * i + 1]) >= 65)
            low = int(Hex[2 * i + 1] - 65 + 10);
        else
            low = int(Hex[2 * i + 1] - 48);

        Char[i] = char(high * 16 + low);
    }
    return Char;
}

string RC4::rc4(const string &textstr)
{
    const char *text = textstr.c_str();
    string ret;
    init(keystr);
    int length = 0;
    length = strlen(text);
    int i, j;
    i = 0, j = 0;

    for (int p = 0; p < length; p++)
    {
        i = (i + 1) % 256;
        j = (j + S[i]) % 256;
        int tmp;
        tmp = S[j];
        S[j] = S[i];
        S[i] = tmp;
        int k = S[(S[i] + S[j]) % 256];
        ret += (text[p] ^ k);
        // text[p] = text[p] ^ k;
    }
    return ret;
}

RC4::~RC4()
{
}

int main(int argc, char const *argv[])
{
    string A = "UOIzVDP2Vzs=";
    const char* B = "UOIzVDP2Vzs=";
    string key = "flag{this_is_not_the_flag_hahaha}";
    RC4 rc4(key);
    cout << A << endl;
    string ret = rc4.rc4(A);
    string rets = rc4.rc4(B);
    cout << ret << endl;
    cout << rc4.char_2_Hex(ret.c_str()) << endl;
    string ret2 = rc4.rc4(ret);
    cout << ret2 << endl;
    return 0;
}
