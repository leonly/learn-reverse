/*
 * @Descripttion: 
 * @version: 
 * @Author: Leo
 * @Date: 1970-01-01 08:00:00
 * @Last Modified by: Leo
 * @Last Modified time: Do not Edit
 */
#include <cstdio>
#include "sha256_literal.h"
#include <iostream>

// using namespace std;
using sha256_literal::compute;
using sha256_literal::compute_str;
using sha256_literal::HashType;

static constexpr bool eq(HashType const A, HashType const B)
{
    for (size_t i = 0; i < std::tuple_size<HashType>(); ++i)
    {
        if (A[i] != B[i])
        {
            return false;
        }
    }
    return true;
}

int test()
{
    const char pwd[] = "myverysecretpassword";
    static constexpr auto PasswordHash = "myverysecretpassword"_sha256;
    if (compute_str(pwd) == PasswordHash)
    {
        puts("good password!");
        for (uint8_t i = 0; i < sizeof(PasswordHash)/ sizeof(uint8_t); i++)
        {
            std::cout << std::hex << ((char) PasswordHash[i]);
        }
        return 0;
    }
    puts("bad password!");
    return 1;
}
