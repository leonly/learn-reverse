# _*_ coding: utf-8 _*_
#!/usr/bin/python2.7
'''
Descripttion: 
version: 
Author: Leo
Date: 1970-01-01 08:00:00
Last Modified by: Leo
Last Modified time: Do not Edit
'''

import collections
import idautils
import idc
import json


def genJsonFile(libName, path):
    ea = idc.ScreenEA()
    # 得到所有指令地址集合
    dism_addr = list(idautils.FuncItems(ea))
    reJson = {}
    configList = []
    for curr_addr in dism_addr:
        if(idc.GetMnem(curr_addr).lower() in ['b', 'blx', 'bl'] and not idc.GetOpnd(curr_addr, 0).startswith("loc")):
            configList.append(genJson(libName, curr_addr))
    reJson["ConfigList"] = configList
    reJson["Type"] = "NATIVE"
    saveJson(reJson, path)


def genJson(libName, addr):
    reJson = {}
    reJson['ModelName'] = libName
    reJson['Address'] = hex(addr).replace('L', '')
    od = collections.OrderedDict()
    od["stackInfo_native"] = {
        "MemPrint": "false",
        "MemU64Print": "false",
        "MemLen": 0,
        "Mem64Len": 0,
        "ValuePrint": "true"
    }
    od["retVal_native"] = {
        "MemPrint": "false",
        "MemU64Print": "false",
        "MemLen": 0,
        "Mem64Len": 0,
        "ValuePrint": "true"
    }
    od["param0_native"] = {
        "MemPrint": "false",
        "MemU64Print": "false",
        "MemLen": 1000,
        "Mem64Len": 300,
        "ValuePrint": "true"
    }
    od["param1_native"] = {
        "MemPrint": "false",
        "MemU64Print": "false",
        "MemLen": 1000,
        "Mem64Len": 300,
        "ValuePrint": "true"
    }
    od["param2_native"] = {
        "MemPrint": "false",
        "MemU64Print": "false",
        "MemLen": 1000,
        "Mem64Len": 300,
        "ValuePrint": "true"
    }
    paramConfig = [
        od
    ]
    reJson["ParamConfig"] = paramConfig
    return reJson


def saveJson(jsond, path):
    with open(path, 'w') as f:
        f.write(json.dumps(jsond))


if __name__ == "__main__":
    path = "result.json"
    libName = "libSrc.so"
    print("gen json start")
    genJsonFile(libName, path)
    print("gen json end")
