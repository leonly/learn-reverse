'''
Author: Leo
Date: 2021-03-28 10:01:08
LastEditors: Leo
LastEditTime: 2021-05-12 15:12:29
Description: 
'''
from ctypes import string_at
from sys import getsizeof
from binascii import hexlify
import sys

from ida import idautils, idaapi, idc
from datetime import datetime


def decryFunc():
    ea = idc.ScreenEA()
    beforeea = ea-5*4
    str = idc.GetOpnd(beforeea, 0)
    addr = ''
    length = ''
    while beforeea <= ea:
        str = idc.GetOpnd(beforeea, 1)
        op = idc.GetMnem(beforeea)
        str0 = idc.GetOpnd(beforeea, 0)
        if 'decry' not in idc.GetOpnd(ea, 0):
            print('not decry func')
            break
        #comment=idc.GetCommentEx(beforeea, 1)
        # print comment
        if 'unk_' in str and '@' in str:
            addr = str[str.index('unk_')+4:str.index('@')]
            # print addr
        if 'MOV' in op and 'W1' in str0:
            length = str[str.index('#')+1:]
            # print length
        beforeea = beforeea+4

    if len(addr) > 0:
        if len(length) > 0:
            # print addr
            mlen = int(length, 16)
            keyaddr = int(addr, 16)+int(length, 16)
            tmp = idc.Byte(keyaddr)-mlen*mlen
            realkey = tmp & 0xff
            for byte in idc.GetManyBytes(int(addr, 16), int(length, 16)):
                # print  "0x%X" % ord(byte),
               # print hex(ord(byte)),
                sys.stdout.write(chr((ord(byte) ^ realkey) & 0x7f))
                realkey = realkey+mlen
                # print realkey
    del addr
    del length
