'''
Author: Leo
Date: 2021-03-28 10:01:08
LastEditors: Leo
LastEditTime: 2021-05-12 15:11:37
Description: 获取当前鼠标位置函数边界
'''

import idautils
import idaapi
import idc

# 获取当前地址
ea = idc.ScreenEA()
ea = here()
# 反汇编窗口中
idc.SegName(ea)  # get text
idc.GetDisasm(ea)  # 获取指令和参数 mov a, b
idc.GetMnem(ea)  # 获取指令  mov
idc.GetOpnd(ea, 0)  # 第一个参数 a
idc.GetOpnd(ea, 1)  # 第二个参数 b

# 获取所以段信息
for seg in idautils.Segments():
    segname = idc.SegName(seg)
    start_ea = idc.SegStart(seg)
    end_ea = idc.SegEnd(seg)

# 所有函数列表
for fuc in idautils.Functions():
    fuc_ea = hex(fuc)
    name = idc.GetFunctionName(fuc)

func = idaapi.get_func(ea)  # 获取函数边界
func.startEA, func.endEA
