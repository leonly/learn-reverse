'''
Author: Leo
Date: 2021-05-27 11:36:15
LastEditors: Leo
LastEditTime: 2021-05-28 15:23:58
Description: 通过riruillcppdumer dump出来的dump.cs文件，生成il2cppdumper的script.json文件，方便ida导入
'''
import re
import json
from typing_extensions import Protocol


func_prog = re.compile('.+\\(.*\\) *\\{.*\\}$')

namespace_dict = {}

script = {}
ScriptMethod = []
Addresses = []
structs = []
last_line = ""

pre_class = {
    "string": "System_String",
    "object": "System_Object",
    "Exception": "System_Exception",
    "bool": "bool",
    "Boolean": ""
}


def fill_namespace(line):
    if line.startswith("// Namespace:"):
        w = line.split(' ')
        if len(w) > 2:
            namespace_dict[w[2]] = 1


def fill_method(line):
    f = func_prog.match(line)
    if f is not None:
        fstr = f.group()


def deal_with_dumpcs(path):
    with open(path, 'r') as f:
        lines = f.readlines()
        for line in lines:
            line = line.strip()
            if line == '':
                continue
            fill_namespace(line)

            last_line = line
            pass


if __name__ == '__main__':
    pass
