<!--
 * @Author: Leo
 * @Date: 2021-05-13 13:42:06
 * @LastEditors: Leo
 * @LastEditTime: 2021-08-22 13:20:57
 * @Description: 
-->
# unity 3D il2cppdumper正确食用过程 #
[il2cppdumper正确食用过程 ](https://floe-ice.cn/archives/96)

il2cppdumper dump下来后头文件修复过程：
1. 修改program_header table中的所有 Loadable Segment中Elf64_Addr p_vaddr_VIRTUAL_ADDRESS和Elf64_Addr p_paddr_PHYSICAL_ADDRESS为0，
2. 修改program_header table中的所有 Loadable Segment中Elf64_Xword p_filesz_SEGMENT_FILE_LENGTH和Elf64_AdElf64_Xword p_filesz_SEGMENT_FILE_LENGTH为文件长度。

## 手动寻找coderegistration
1. Ctrl+S打开segment list,双击.init_array，里面是初始化函数列表
2. 双击每一个函数，查看内容，找到和如下相似的函数：  
.text:0000000000B4FD18 ; __unwind {
.text:0000000000B4FD18                 ADRP            X0, #unk_2FDFD8B@PAGE
.text:0000000000B4FD1C                 ADRP            X1, #sub_D1DB7C@PAGE
.text:0000000000B4FD20                 ADD             X0, X0, #unk_2FDFD8B@PAGEOFF
.text:0000000000B4FD24                 ADD             X1, X1, #sub_D1DB7C@PAGEOFF
.text:0000000000B4FD28                 MOV             X2, XZR
.text:0000000000B4FD2C                 MOV             W3, WZR
.text:0000000000B4FD30                 B               loc_D67FC8
.text:0000000000B4FD30 ; } // starts at B4FD18
3. 或者如果有一个函数明显和其它的不同，则它很可能就是我们要找到code_reg_hook
4. 如果有函数里面包含“__cxa_atexit，__cxa, __gxx”，则可以立刻排除
5. 找到之后按N命名，code_reg_hook
6. 进入最后跳转的函数loc_D67FC8，它是RegisterRuntimeInitializeAndCleanup
7. ADRP loads the top 32 bits of the address into a register
8. 猜测x0,x1都是指针，重命名之后如下：  
.text:0000000000B4FD18 code_reg_hook
.text:0000000000B4FD18 ; __unwind {
.text:0000000000B4FD18                 ADRP            X0, #this@PAGE
.text:0000000000B4FD1C                 ADRP            X1, #Il2CppCodeGenRegistration@PAGE
.text:0000000000B4FD20                 ADD             X0, X0, #this@PAGEOFF
.text:0000000000B4FD24                 ADD             X1, X1, #Il2CppCodeGenRegistration@PAGEOFF
.text:0000000000B4FD28                 MOV             X2, XZR
.text:0000000000B4FD2C                 MOV             W3, WZR
.text:0000000000B4FD30                 B               loc_D67FC8
.text:0000000000B4FD30 ; } // starts at B4FD18
9. 双击进入il2cppcoderegistration，如下：  
.text:0000000000D1DB7C Il2CppCodeGenRegistration
.text:0000000000D1DB7C ; __unwind {
.text:0000000000D1DB7C                 ADRP            X1, #off_2DB5048@PAGE
.text:0000000000D1DB80                 LDR             X1, [X1,#off_2DB5048@PAGEOFF]
.text:0000000000D1DB84                 ADRP            X0, #unk_2D40460@PAGE
.text:0000000000D1DB88                 ADRP            X2, #unk_24DF3DC@PAGE
.text:0000000000D1DB8C                 ADD             X0, X0, #unk_2D40460@PAGEOFF
.text:0000000000D1DB90                 ADD             X2, X2, #unk_24DF3DC@PAGEOFF
.text:0000000000D1DB94                 B               loc_D71E34
重命名如下：  
.text:0000000000D1DB7C Il2CppCodeGenRegistration
.text:0000000000D1DB7C ; __unwind {
.text:0000000000D1DB7C                 ADRP            X1, #g_MetadataRegistration@PAGE
.text:0000000000D1DB80                 LDR             X1, [X1,#g_MetadataRegistration@PAGEOFF]
.text:0000000000D1DB84                 ADRP            X0, #g_CodeRegistration@PAGE
.text:0000000000D1DB88                 ADRP            X2, #s_Il2CppCodeGenOptions@PAGE
.text:0000000000D1DB8C                 ADD             X0, X0, #g_CodeRegistration@PAGEOFF
.text:0000000000D1DB90                 ADD             X2, X2, #s_Il2CppCodeGenOptions@PAGEOFF
.text:0000000000D1DB94                 B               il2cpp_codegen_register
10. x0,x1所在位置即我们要找到registration，


## 手动寻找coderegistration2
1. 搜索函数il2cpp_class_get_methods.
2. 根据il2cpp_class_get_methods->GetMethods->SetupMethods->SetupMethodsLocked一直追踪到SetupMethodsLocked方法。
3. 具体见下图：
![alt 第一步](./img/1.png "可选标题")
![alt 第二步](./img/2.png "可选标题")
![alt 第三步](./img/3.png "可选标题")
![alt 第四步](./img/4.png "可选标题")


tips:  DCB分配一段字节的内存单元，其后的每个操作数都占有一个字节，操作数可以为-128～255的数值或字符串
DCW分配一段半字的内存单元，其后的每个操作数都占有两个字节，操作数是16位二进制数，取值范围为-32768～65535
DCD分配一段字的内存单元，其后的每个操作数都占有4个字节，操作数可以是32位的数字表达式，也可以是程序中的标号（因为程序中的标号代表地址，也是32位二进制数值）
DCQ分配一段双字的内存单元，其后的每个操作数都占有8个字节 
按D进行切换展示。