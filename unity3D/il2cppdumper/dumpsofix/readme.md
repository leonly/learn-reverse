<!--
 * @Author: Leo
 * @Date: 2021-06-18 18:38:36
 * @LastEditors: Leo
 * @LastEditTime: 2021-06-18 18:40:09
 * @Description: 
-->

# 编译c++程序
你可以使用本演练中的步骤来创建自己的 C++ 代码，而不是键入所示的示例代码。 还可通过这些步骤生成你在其他位置看到的许多 C++ 代码示例程序。 你可以在任何可写目录放置源代码并生成应用。 默认情况下，Visual Studio IDE 在用户文件夹的“source\repos”子文件夹中创建项目。 旧版本可能会将项目放入 Documents\Visual Studio <version>\Projects* 文件夹中。

若要编译包含其他源代码文件的程序，请在命令行上将它们全部输入，例如：

cl /EHsc file1.cpp file2.cpp file3.cpp

/EHsc 命令行选项指示编译器启用标准 C++ 异常处理行为。 如果没有它，则引发的异常可能导致未受损对象和资源泄漏。 有关详细信息，请参阅 /EH（异常处理模型）。

提供其他源文件时，编译器会使用第一个输入文件创建程序名。 在本例中，编译器输出一个名为 file1.exe 的程序。 若要将名称更改为 program1.exe，请添加 /out 链接器选项：

cl /EHsc file1.cpp file2.cpp file3.cpp /link /out:program1.exe

若要自动捕获更多编程错误，我们建议使用 /W3 或 /W4 警告级别选项进行编译：

cl /W4 /EHsc file1.cpp file2.cpp file3.cpp /link /out:program1.exe

# 编译c程序
你可以使用本演练中的步骤生成自己的 C 代码，而不是键入所示的示例代码。 你还可以生成在其他位置看到的许多 C 代码示例程序。 若要编译包含其他源代码文件的程序，请在命令行上将它们全部输入，例如：

cl file1.c file2.c file3.c

编译器输出名为 file1.exe 的程序。 若要将名称更改为 program1.exe，请添加 /out 链接器选项：

cl file1.c file2.c file3.c /link /out:program1.exe

若要自动捕获更多编程错误，我们建议使用 /W3 或 /W4 警告级别选项进行编译：

cl /W4 file1.c file2.c file3.c /link /out:program1.exe

编译器 cl.exe 具有更多可用于生成、优化、调试和分析代码的选项。 如需快速列表，请在开发人员命令提示下输入 cl /?。 你还可以单独编译和链接，并在更复杂的生成方案中应用链接器选项。 有关编译器和链接器选项及用法的详细信息，请参阅 C/C++ 生成参考。