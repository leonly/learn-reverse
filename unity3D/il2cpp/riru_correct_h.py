

import collections
import sys

sys.setrecursionlimit(300)


typelist = ["void", "bool", "uint16_t", "int8_t", "uint8_t", "int16_t",
            "int32_t", "uint32_t", "int64_t", "uint64_t", "float", "double", "unsigned", "char"]
# deal file

new_lines = []

new_order = collections.OrderedDict()


def deal_file(filename, newfile):
    with open(filename, 'r', encoding='utf-8') as f:
        lines = f.readlines()
    struct_dict = collections.OrderedDict()
    one_s = []
    hasq = False
    inq = False
    for i in range(len(lines)):
        line = lines[i]
        tr = line.strip().replace('[]', '*').replace('`',
                                                     '_').replace("T* ", "void* ").replace('<>', '')
        if tr.startswith("typedef"):
            new_lines.append(tr)
            continue
        if tr == "":
            continue

        if hasq and '{' in tr:
            inq = True

        if not hasq and '{' in tr:
            hasq = True

        one_s.append(tr)
        if not inq and tr.startswith("struct "):
            key = tr[6:].strip(' {;')
            struct_dict[key] = one_s
        if not inq and tr.startswith("enum "):
            key = tr[5:].strip(' {;')
            struct_dict[key] = one_s
        if not inq and tr.startswith("union "):
            key = tr[6:].strip(' {;')
            struct_dict[key] = one_s
        if hasq and (not inq) and "};" in tr:
            hasq = False
            one_s = []
        if inq and "};" in tr:
            inq = False
        if (not hasq) and tr.endswith(";"):
            one_s = []

    befor = []
    for k in struct_dict.keys():
        befor.append(k)
        dealone(k, struct_dict, befor)
        befor = []

    for k, v in new_order.items():
        for line in v:
            new_lines.append(line)

    with open(newfile, 'w', encoding='utf-8') as f:
        f.writelines("\n".join(new_lines))


def dealone(k, d, befor):
    if k in new_order:
        return
    if k not in d:
        new_order[k] = ["struct " + k, '{', '};']
        return
    if ":" in k:
        s = k.split(" : ")
        for i in range(1, len(s)):
            befor.append(s[i])
            dealone(s[i], d, befor)
    v = d[k]
    for line in v:
        if line.startswith("const"):
            line = line[6:]
        if line.startswith("struct") or line.startswith("enum") or line.startswith("union"):
            continue
        ls = line.split(" ")
        nt = ls[0].strip('*[]')
        if nt in typelist:
            continue
        if nt in befor:
            continue
        if len(ls) > 1:
            befor.append(nt)
            if nt == 'System_Collections_Generic_Dictionary_2':
                a = 5
            dealone(nt, d, befor)
    new_order[k] = d[k]
    print("insert: " + k)


if __name__ == '__main__':
    print("start..............")
    file = "D:\\mywork\\moer\\v1-moer\\il2cpp.h"
    new_file = "D:\\mywork\\moer\\v1-moer\\il2cpp_new.h"
    # file = "D:\\mywork\\jiangnan\\1.4.3\\il2cpp.h"
    # new_file = "D:\\mywork\\jiangnan\\1.4.3\\il2cpp_new.h"
    deal_file(file, new_file)
    print("end..............")
