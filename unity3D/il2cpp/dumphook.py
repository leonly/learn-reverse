#!/usr/bin/env python
# _*_ coding: utf-8 _*_

'''
Descripttion: hook
version: 1
Author: Leo
Date: 2021-01-17 22:16:06
LastEditor: Leo
LastEditTime: 2021-04-11 17:43:19
'''
import frida
import threading
import time

g_event = threading.Event()


def payload_message(payload):
    if 'msg' in payload:
        print(payload['msg'])
    if 'status' in payload:
        if payload['status'] == 'success':
            g_event.set()


# 连接安卓机上的frida-server
device = frida.get_usb_device()
pid = device.spawn(['com.cis.jiangnan.taptap'])
device.resume(pid)
time.sleep(0.5)
session = device.attach(pid)
with open("dumpstrings.js") as f:
    script = session.create_script(f.read())


f = open("funs_dumps.txt", 'w')


# 定义消息处理
def on_message_handler(message, payload):
    print(f"message: {message}")
    f.write(message['payload'])
    f.write('\n')
    f.flush()


script.on("message", on_message_handler)  # 调用错误处理
script.load()
# 脚本会持续运行等待输入
input()
f.close()
