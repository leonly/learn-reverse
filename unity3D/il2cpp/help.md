<!--
 * @Author: Leo
 * @Date: 2021-05-12 13:18:06
 * @LastEditors: Leo
 * @LastEditTime: 2021-05-12 18:07:24
 * @Description: 
-->

## 常用网址
### untiy相关符号还原文章
1. [unity][Real-time Dump](https://floe-ice.cn/archives/502)
2. [IL2CPP的原理](https://blog.csdn.net/feibabeibei_beibei/article/details/95922520)
3. [还原使用IL2CPP编译的unity游戏的symbol（一）](https://www.nevermoe.com/2016/08/10/unity-metadata-loader/)
4. [还原使用IL2CPP编译的unity游戏的symbol（二）](https://www.nevermoe.com/2016/09/08/%e8%bf%98%e5%8e%9f%e4%bd%bf%e7%94%a8il2cpp%e7%9a%84symbol%ef%bc%88%e4%ba%8c%ef%bc%89/)
5. [内存解析Il2cpp函数地址](https://buaq.net/go-53725.html)
6. [unity+xlua的架构实例](https://floe-ice.cn/archives/544)
7. [[Unity]正确食用Il2cppdumper](https://floe-ice.cn/archives/96)

### 一些工具文章
1. [安卓逆向工具汇总(附下载地址)](https://www.freebuf.com/sectool/248409.html)
2. [使用dumpDex脱壳app](https://blog.csdn.net/qq_32562005/article/details/109618520)