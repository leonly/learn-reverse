'''
Author: Leo
Date: 2021-06-10 16:44:35
LastEditors: Leo
LastEditTime: 2021-08-20 17:53:27
Description: riru dump script.json文件相对路径不对，通过此脚本进行修复
'''

import json


# deal file
def deal_file(filename, newfile, base):

    data = json.loads(open(filename, 'rb').read().decode('utf-8'))

    if "ScriptMethod" in data:
        scriptMethods = data["ScriptMethod"]
        for scriptMethod in scriptMethods:
            addr = scriptMethod["AbsAddress"]
            scriptMethod["Address"] = addr - int(base, 16)
        data["ScriptMethod"] = scriptMethods

    if "ScriptString" in data:
        scriptStrings = data["ScriptString"]
        for scriptString in scriptStrings:
            addr = scriptString["AbsAddress"]
            scriptString["Address"] = addr - int(base, 16)
        data["ScriptString"] = scriptStrings

    with open(newfile, 'w', encoding='utf-8') as f:
        f.write(json.dumps(data, indent=4, separators=(',', ': ')))


if __name__ == '__main__':
    print("start..............")
    new_base_addr = '0x7c7cb1b000'
    # file = "D:\\mywork\\jiangnan\\1.4.2\\dump.cs"
    file = "D:\\mywork\\jiangnan\\1.5.1\\script.json"
    new_file = "D:\\mywork\\jiangnan\\1.5.1\\script_new.json"
    deal_file(file, new_file, new_base_addr)
    print("end..............")
