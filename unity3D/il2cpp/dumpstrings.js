/*
 * @Descripttion: 
 * @version: 
 * @Author: Leo
 * @Date: 2021-03-10 23:49:51
 * @LastEditor: Leo
 * @LastEditTime: 2021-04-11 21:55:56
 */
// hook SetupMethodsLocked
function dumpString() {
    var module = Module.findBaseAddress("libil2cpp.so");
    var p_size = 8;
    console.log(module)
    Interceptor.attach(module.add(0x13d968c), { //12A992C SetupMethodsLocked函数
        onEnter: function (args) {
            // console.log(this.context.x0)
            // var methodsDefinition = this.context.x0;
            var newMethod = this.context.x20.sub(0x50)
            var pointer = newMethod.readPointer(); //MethodInfo
            var name = newMethod.add(p_size * 2).readPointer().readCString();
            var klass = newMethod.add(p_size * 3).readPointer(); //Il2CppClass
            var klass_name = klass.add(p_size * 2).readPointer().readCString();
            var klass_paze = klass.add(p_size * 3).readPointer().readCString();
            // console.log(klass_paze + "." + klass_name + ":" + name + "    -> " + pointer.sub(module));
            send(klass_paze + "." + klass_name + ":" + name + "-> " + pointer.sub(module));
        }
    });
}
dumpString()