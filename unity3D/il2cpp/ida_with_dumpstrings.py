#!/usr/bin/env python2
# -*- coding:utf-8  -*-

'''
Author: Leo
Date: 2021-04-11 18:05:02
LastEditors: Leo
LastEditTime: 2021-05-12 15:08:54
Description: 通过Hook SetupMethodsLocked 进行方法string dump的，生成的文件，通过本脚本导入ida，重命名方法
'''


imageBase = idaapi.get_imagebase()


def get_addr(addr):
    return imageBase + addr


def set_name(addr, name):
    ret = idc.set_name(addr, name, SN_NOWARN | SN_NOCHECK)
    if ret == 0:
        new_name = name + '_' + str(addr)
        ret = idc.set_name(addr, new_name, SN_NOWARN | SN_NOCHECK)


def make_function(start, end):
    next_func = idc.get_next_func(start)
    if next_func < end:
        end = next_func
    if idc.get_func_attr(start, FUNCATTR_START) == start:
        ida_funcs.del_func(start)
    ida_funcs.add_func(start, end)


def load_methods(path):
    with open(path) as f:
        content = f.readlines()

    for line in content:
        line = line.strip(' ').replace(
            ' ', '').replace('\r', '').replace('\n', '')
        new_line = line.split('->')
        i = 0
        if len(new_line) < 2 or len(new_line[1]) > 9:
            continue
        addr = get_addr(int(new_line[1], 16))
        name = new_line[0]
        set_name(addr, name)
        idc.set_func_cmt(addr, line, 0)
        i += 1


path = idaapi.ask_file(False, '*.txt', 'funs_dump from dumpstrings')
load_methods(path)

print('Script finished!')
