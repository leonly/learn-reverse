'''
Author: Leo
Date: 2021-03-30 23:36:18
LastEditors: Leo
LastEditTime: 2021-06-18 16:09:50
Description: riru dump出来的dump.cs路径计算错误，riru基地址使用的是壳地址。
修复过程：cat /proc/pid/maps | grep ill2cpp
获取真实基地址，修改后执行本脚本
'''


# deal file
def deal_file(filename, newfile, base):
    with open(filename, 'r', encoding='utf-8') as f:
        lines = f.readlines()

    for i in range(len(lines)):
        line = lines[i]
        tr = line.strip()
        if tr.startswith("// RVA:"):
            old_addr = tr.split(" ")[2]
            addr = tr.split(" ")[4]
            new_addr = hex(int(addr, 16) - int(base, 16))
            line = line.replace(old_addr, new_addr)
            lines[i] = line

    with open(newfile, 'w', encoding='utf-8') as f:
        f.writelines(lines)


if __name__ == '__main__':
    print("start..............")
    new_base_addr = '0x7034c96000'
    # file = "D:\\mywork\\jiangnan\\1.4.2\\dump.cs"
    file = "D:\\mywork\\jiangnan\\1.4.3\\dump.cs"
    new_file = "D:\\mywork\\jiangnan\\1.4.3\\dump_new.cs"
    deal_file(file, new_file, new_base_addr)
    print("end..............")
