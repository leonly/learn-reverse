function dump_memory(start,end,name) {
    Java.perform(function () {
        var currentApplication = Java.use("android.app.ActivityThread").currentApplication();
        var dir = currentApplication.getApplicationContext().getFilesDir().getPath();
        var file_path = dir + "/" + name;
        var file_handle = new File(file_path, "wb");
        var size = end - start;
        if (file_handle && file_handle != null) {
            Memory.protect(ptr(start), size, 'rwx');
            var libso_buffer = ptr(start).readByteArray(size);
            file_handle.write(libso_buffer);
            file_handle.flush();
            file_handle.close();
            console.log("[dump]:", file_path);
        }
    });
}